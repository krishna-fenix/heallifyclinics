import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { CommonServiceService } from "../../services/common/common-service.service";
import { LoginService } from '../../services/login.service';

@Component({
	selector: 'app-side-navigation',
	templateUrl: './side-navigation.component.html',
	styleUrls: ['./side-navigation.component.scss']
})
export class SideNavigationComponent implements OnInit {


  	constructor(
		private _router:Router,
		private loginService: LoginService,
		private mLocalStorage: LocalStorageService,
		private commonService:CommonServiceService
	) { }


	ngOnInit() {

	}

	navigateToAppointment(){
		this.commonService.navigateByUrl('/dashboard/appointment');
	}

	OnLogOut() {
		this.loginService.logOut().subscribe(
			() => {
				this.mLocalStorage.clearAll();
				this.commonService.mUser = undefined;
				this.commonService.navigateByUrl('/login');
			}, error => {
				this.mLocalStorage.clearAll();
				this.commonService.mUser = undefined;
			// this.showError(error, '');
				this.commonService.navigateByUrl('/login');
			}
		);

  	}

}
