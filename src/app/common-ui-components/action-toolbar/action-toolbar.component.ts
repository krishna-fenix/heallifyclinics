import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-action-toolbar',
  templateUrl: './action-toolbar.component.html',
  styleUrls: ['./action-toolbar.component.scss']
})
export class ActionToolbarComponent implements OnInit {

  constructor(private _router:Router) { }

  ngOnInit() {
  }

  navigateToNewAppointment(){
    // this._router.navigateByUrl('dashboard/addpatient')
  }

}
