import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-validations',
  templateUrl: './validations.component.html',
  styleUrls: ['./validations.component.scss']
})
export class ValidationsComponent implements OnInit {
  
  @Input() validatingFields: any;
  @Input() passwordPattern: string;

  constructor() { }

  ngOnInit() {
  }

}
