import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { NavDraweroperatorService } from 'src/app/services/drawer/nav-draweroperator.service';
import { LocalStorageService } from 'angular-2-local-storage';
import { LoginService } from '../../services/login.service';
import { CommonServiceService } from "../../services/common/common-service.service"

@Component({
	selector: 'app-navdrawer',
	templateUrl: './navdrawer.component.html',
	styleUrls: ['./navdrawer.component.scss']
})
export class NavdrawerComponent implements OnInit {
	mUser: any;

	tLinks = [
		{ route: '/dashboard/clinic-dashboard', icon: 'home', title: 'Dashboard' },
		{ route: '/dashboard/appointment', icon: 'watch_later', title: 'Appointment' },
		{ route: '/dashboard/patientlist', icon: 'list', title: 'Patient List' },		
		{ route: '/dashboard/dailyreports', icon: 'assignment_ind', title: 'Reports' },
		// { route: '/dashboard/staff', icon: 'people', title: 'Staff' }
	];

	bLinks = [
		{ route: '/dashboard/profilepage', icon: 'settings_applications', title: 'Settings' },
		{ route: 'logout', icon: 'power_settings_new', title: 'Logout', action: 'OnLogOut()' }
	];

	mDrawerShrink = false;

	@Input()
	open: boolean;


	@Output()
	sidenav: EventEmitter<any> = new EventEmitter<any>();


	constructor(
		private commonService:CommonServiceService,
		public drawerService: DrawerService,
		public mDrawerOpertor: NavDraweroperatorService,
		private mLocalStorage: LocalStorageService,
		private loginService: LoginService
	) {	}

	toggleDrawerShrink() {
		this.drawerService.close();
		this.mDrawerShrink = !this.mDrawerShrink;
	}


	ngOnInit() {
		this.userDetails();
	}

	userDetails() {
		this.mUser = this.commonService.getCurrentUser();
	}

	public OnLogOut(route: string) {
		if (route == "logout") {
			this.loginService.logOut().subscribe(
				() => {
					this.mLocalStorage.clearAll();
					this.commonService.mUser = undefined;
					this.commonService.showInfo('Logged Out', '');
					this.commonService.navigateByUrl('/login');
				}, error => {
					this.mLocalStorage.clearAll();
					this.commonService.mUser = undefined;
					this.commonService.showError(error, '');
					this.commonService.navigateByUrl('/login');
				}
			);
		}
	}
}
