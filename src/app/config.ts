import { environment } from './../environments/environment';

// Site Configurations goes here
const APIHOST = environment.production ? 'https://api.heallify.com' : 'https://api.heallify.com';
export const CONFIG = {
    is_hospitaladmin: false,
    is_doctor: false,
    baseApiHost: APIHOST,
    is_multitenant:false,
    FIREBASE_CONFIG: {
        apiKey: environment.firebase.apiKey,
        authDomain: environment.firebase.authDomain,
        databaseURL: environment.firebase.databaseURL,
        projectId: environment.firebase.projectId,
        storageBucket: environment.firebase.storageBucket,
        messagingSenderId: environment.firebase.messagingSenderId,
        appId: environment.firebase.appId
    },    
    ZOOM_SDK_KEY: environment.zoom.SDK_KEY,
    ZOOM_SDK_SECRET: environment.zoom.SDK_SECRET,
    ZOOM_API_KEY: environment.zoom.API_KEY,
    ZOOM_API_SECRET: environment.zoom.API_SECRET,
    ZOOM_JWT_TOKEN: environment.zoom.JWT_TOKEN,
    ZOOM_LEAVE_URL: environment.production ? 'https://api.heallify.com':'http://localhost:4200/dashboard',
    APPOINTMENT_STATUS:  {
        pending: "1",
        cancelled: "2",
        confirmed: "3",
        verified: "4",
        rescheduled: "5",
        completed: "6",
        noshow: "7",
        requested: "8",
    },
    USER_TYPES:{
        DOCTOR:'2',
        HOSPITAL_ADMIN:'7',
        CLINIC_ADMIN:'8'
    }
}