import { Injectable } from '@angular/core';
import { RESTAPI } from '../../restapi';
import { HttpRequestService } from '../http-request.service';
import { Observable } from 'rxjs/Observable';


@Injectable({
	providedIn: 'root'
})
export class AppointmentService {

	constructor(
		private _http: HttpRequestService
	) { }

	// GET Requests

	getAppointmentInfoById(id):Observable<any>{
		return this._http.get(RESTAPI.GETAPPOINTMENTINFOBYID + id);
	}
	
	getDoctorFaceSheet(appointment_id):Observable<any>{
		return this._http.get(RESTAPI.GETDOCTORFACESHEET + appointment_id);
	}

	getAllDrugs(name?:string):Observable<any>{
		return this._http.get(RESTAPI.GETALLDRUGS + '?name=' + name);
	}

	getAllergiesAutocomplete(allergie_name):Observable<any>{
		return this._http.get(RESTAPI.GETALLERGIESAUTOCOMPLETE + '?name=' + allergie_name);
	}

	getConditionsAutocomplete(condition_name):Observable<any>{
		return this._http.get(RESTAPI.GETCONDITIONSAUTOCOMPLETE + '?name=' + condition_name);
	}

	getAutocompleteForDigitalPrescription():Observable<any>{
		return this._http.get(RESTAPI.AUTOCOMPLETEDIGITALPRESCRIPTION);
	}

	getRelations():Observable<any>{
		return this._http.get(RESTAPI.GETRELATIONSAUTOCOMPELTE);
	}

	getBloogGroups():Observable<any>{
		return this._http.get(RESTAPI.GETBLOODGROUPS);
	}

	getCities():Observable<any>{
		return this._http.get(RESTAPI.GETCITIES);
	}
	
	getCurrentAppointments(): Observable<any>{
		return this._http.get(RESTAPI.GETCURRENTAPPOINTMENT);
	}

	getPastAppointments(): Observable<any> {
		return this._http.get(RESTAPI.GETPASTAPPOINTMENT);
	}

	getAllAppointments(userId?:string): Observable<any> {
		if(userId)
			return this._http.get(RESTAPI.GETALLAPPOINTMENT + '?user_id=' + userId);
		else 
			return this._http.get(RESTAPI.GETALLAPPOINTMENT);
	}

	getAppoitmentReasons():Observable<any>{
		return this._http.get(RESTAPI.GETAPPOINTMENTREASONS);
	}

	getAppointmentReasonsByDoctorId(doctor_id):Observable<any>{
		return this._http.get(RESTAPI.GETREASONSBYDOCTORID + doctor_id);
	}

	getVideoAppointmentStatus(): Observable<any> {
		return this._http.get(RESTAPI.GETVIDEOAPPOINTMENTSTATUS);
	}

	findPatients(query?:string):Observable<any>{
		return this._http.get(RESTAPI.FINDPATIENTS + '?query=' + query);
	}

	getExistingPatient(mobile_number):Observable<any>{
		return this._http.get(RESTAPI.FINDEXISTINGPATIENT + mobile_number);
	}

	// POST Requests
	bookAppointment(newAppointment: any): Observable<any> {
		const body = new FormData();
		for (const key in newAppointment) {
			body.append(key, newAppointment[key])
		}
		return this._http.post(RESTAPI.BOOKINHOUSEAPPOINTMENT, body);
	}

	setAppointmentFollowUp(body):Observable<any>{
		return this._http.post(RESTAPI.SETFOLLOWUP, body);
	}

	addConditions(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDCONDITIONS + appointment_id, body);
	}

	removeConditions(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.REMOVECONDITIONS + appointment_id, body);
	}

	addAllergies(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDALLERGIES + appointment_id, body);
	}

	removeAllergies(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.REMOVEALLERGIES + appointment_id, body);
	}

	addDiagonosis(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDDIAGONOSIS + appointment_id, body);
	}

	addPhysicalExamination(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDPHYSICALEXAMINATION + appointment_id, body);
	}

	addTestNotes(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDDIAGONOSIS + appointment_id, body);
	}

	addLabTests(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDLABTEST + appointment_id, body);
	}

	removeLabTests(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.REMOVELABTESTS + appointment_id, body);
	}

	addLogs(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDLOGS + appointment_id, body);
	}

	reschuleAppointment(body):Observable<any>{
		return this._http.post(RESTAPI.RESCHEDULEAPPOITMENT, body);
	}

	updateAppointmentStatus(status_id, body):Observable<any>{
		return this._http.post(RESTAPI.UPDATEAPPOINTMENTSTATUS + status_id, body);
	}

	createNewPrescription(body):Observable<any>{
		return this._http.post(RESTAPI.CREATENEWPRESCRIPTION, body);
	}

	addPrescriptionDetails(main_prescription_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDPRESCRIPTIONDETAILS + main_prescription_id, body);
	}

	updatePrescription(main_prescription_id, prescription_id, body):Observable<any>{
		return this._http.post(RESTAPI.UPDATEPRESCRIPTIONBYID + main_prescription_id + '/' + prescription_id, body);
	}

	goLiveForVideoAppointment(clinic_id, body):Observable<any>{
		return this._http.post(RESTAPI.GOLIVEFORVIDEOAPPOINTMENTS + clinic_id, body);
	}

	// DELETE REQUEST

	removePrescription(main_prescription_id, prescription_id):Observable<any>{
		return this._http.delete(RESTAPI.DELETEPRESCRIPTIONBYID + main_prescription_id + '/' + prescription_id);
	}

	removeFullPrescription(main_prescription_id):Observable<any>{
		return this._http.delete(RESTAPI.DELETEFULLPRESCRIPTIONBYID + main_prescription_id);
	}
	// Hopsital admin get appointment

	getHAAllAppointments(url) {
		return this._http.get(url);
	}



}
