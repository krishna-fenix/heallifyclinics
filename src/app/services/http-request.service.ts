import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  	providedIn: 'root'
})
export class HttpRequestService {

  	constructor(
		private httpClient:HttpClient
	) { }
	
	/**
	* 	HTTP GET REQUEST CALL
	* @params - url - should be in string format
	* reqParams - should be an object like 
		{ 
			params: {
				key:value // value should be in string format
			}
		}
	*/

	get(url: string, reqParams?: Object){
		if(!reqParams)
			reqParams = {};
		return this.httpClient.get(url, reqParams);
	}

	/**
	* 	HTTP POST REQUEST CALL
	* @params - url - should be in string format
	* body - can be of any type
	* reqParams - should be an object like 
		{ 
			params: {
				key:value // value should be in string format
			}
		}
	*/

	post(url: string, body:any, reqParams?: any){
		if(!reqParams)
			reqParams = {};
		return this.httpClient.post(url, body, reqParams);
	}

	/**
	* 	HTTP PUT REQUEST CALL
	* @params - url - should be in string format
	* body - can be of any type
	* reqParams - should be an object like 
		{ 
			params: {
				key:value // value should be in string format
			}
		}
	*/

	put(url:string, body:any, reqParams?:any){
		if(!reqParams)
			reqParams = {};
		return this.httpClient.put(url, body, reqParams);
	}

	/**
	* 	HTTP DELETE REQUEST CALL
	* @params - url - should be in string format
	* reqParams - should be an object like 
		{ 
			params: {
				key:value // value should be in string format
			}
		}
	*/

	delete(url:string, reqParams?:any){
		if(!reqParams)
			reqParams = {};
		return this.httpClient.delete(url, reqParams);
	}
}
