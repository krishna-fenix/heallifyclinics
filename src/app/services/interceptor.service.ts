import { Injectable } from '@angular/core';
import {tap} from 'rxjs/operators';
import {
  	HttpRequest,
  	HttpHandler,
  	HttpEvent,
  	HttpInterceptor,
  	HttpResponse,
  	HttpErrorResponse,
} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {Router} from "@angular/router";
import {CommonServiceService} from './common/common-service.service';

@Injectable({
  	providedIn: 'root'
})
/**
 * InterceptorService - Handles the http requests
 * pre-process the http request before making call
 * and add access token to the request to validate 
 * then
 * post-process the request on next handle after getting the
 * response from the server, generally loggin the request on
 * success and handling the error if any error occur
 */
export class InterceptorService implements HttpInterceptor {

  	constructor(private router: Router, private commonService:CommonServiceService) { }

  	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		// how to update the request Parameters
		if (this.commonService.getCurrentUser() != null) {
			request= request.clone({ 
				setHeaders: {
					access_token: this.commonService.getCurrentUser().token
				}
			});
		}

		return next.handle(request).pipe(
			tap(
				event => {
				// logging the http response to browser's console in case of a success
					if (event instanceof HttpResponse) {
						console.log('api call success :', event);
					}
				},
				error => {
				// logging the http response to browser's console in case of a failuer
					if (error instanceof HttpErrorResponse) {
						console.log('api call error :', error);
						if ( error.status === 401 && !window.location.pathname.includes("/login") ) {
							this.commonService.showError("Logged out", "Please login again");
							// const patharray = window.location.pathname.split('/');
							this.router.navigate(["login"]);
						}
					}
				},
			),
		);
	}
}
