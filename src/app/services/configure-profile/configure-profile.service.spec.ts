import { TestBed } from '@angular/core/testing';

import { ConfigureProfileService } from './configure-profile.service';

describe('ConfigureProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigureProfileService = TestBed.get(ConfigureProfileService);
    expect(service).toBeTruthy();
  });
});
