import { Injectable } from '@angular/core';
import { RESTAPI } from '../../restapi';
import { Observable } from 'rxjs';
import { HttpRequestService } from '../http-request.service';

@Injectable({
	providedIn: 'root'
})
export class ConfigureProfileService {

	constructor(
		private _http: HttpRequestService
	) { }

	// GET Requests
	getAutocomplete(): Observable<any> {
		return this._http.get(RESTAPI.AUTOCOMPLETE);
	}

	// POST Requests

	verifyEmail(emailDetails) {
		const body = new FormData();
		for (const key in emailDetails) {
			body.append(key, emailDetails[key]);
		}
		return this._http.post(RESTAPI.VERIFYEMAIL, body)
	}

	verifyMobile(mobileDetails) {
		const body = new FormData();
		for (const key in mobileDetails) {
			body.append(key, mobileDetails[key]);
		}
		return this._http.post(RESTAPI.VERIFYMOBILE, body)
	}

	sendVerificationCodeOnMobileChange(body): Observable<any> {
		return this._http.post(RESTAPI.CHANGEMOBILESENDCODE, body);
	}

	verifyChangedMobileNumber(body): Observable<any> {
		return this._http.post(RESTAPI.VERIFYCHANGEDMOBILENUMBER, body);
	}

	addProfileDetails(body: any): Observable<any> {
		return this._http.post(RESTAPI.ADDPROFILEDETAILS, body);
	}

	addEducation(body: any): Observable<any> {
		return this._http.post(RESTAPI.ADDEDUCATION, body);
	}

	updateEducationById(id, body): Observable<any> {
		return this._http.post(RESTAPI.UPDATEEDUCATIONBYID + id, body);
	}

	addSpecialization(body): Observable<any> {
		return this._http.post(RESTAPI.ADDSPECIALIZATION, body)
	}

	addService(body): Observable<any> {
		return this._http.post(RESTAPI.ADDSERVICE, body)
	}

	addLanguage(body): Observable<any> {
		return this._http.post(RESTAPI.ADDLANGUAGE, body)
	}

	addMembership(body): Observable<any> {
		return this._http.post(RESTAPI.ADDMEMBERSHIP, body)
	}

	addGallery(body): Observable<any> {
		return this._http.post(RESTAPI.ADDGALLERY, body)
	}

	uploadProfileImage(body): Observable<any> {
		return this._http.post(RESTAPI.UPLOADPROFILEIMAGE, body);
	}

	updateAdminProfile(adminForm) {
		const body = new FormData();
		for (const key in adminForm) {
			body.append(key, adminForm[key]);
		}
		return this._http.post(RESTAPI.UPDATEPROFILE, body);
	}

	// DELETE Requests
	removeSpecializationById(id): Observable<any> {
		return this._http.delete(RESTAPI.DELETESPECIALIZATIONBYID + id);
	}

	removeEducationById(id): Observable<any> {
		return this._http.delete(RESTAPI.DELETEEDUCATIONBYID + id);
	}

	removeServiceById(id): Observable<any> {
		return this._http.delete(RESTAPI.DELETESERVICEBYID + id);
	}

	removeLanguageById(id): Observable<any> {
		return this._http.delete(RESTAPI.DELETELANGUAGEBYID + id);
	}

	removeMembershipById(id): Observable<any> {
		return this._http.delete(RESTAPI.DELETEMEMBERSHIPBYID + id);
	}


}
