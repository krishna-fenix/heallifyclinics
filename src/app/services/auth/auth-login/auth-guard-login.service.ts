import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CommonServiceService } from '../../common/common-service.service';

@Injectable({
    providedIn: 'root'
})

/***
* AuthGuardLoginService
* Authenticate the routes first and then 
* redirect to appropriate route, only when 
* User has logged in
*/

export class AuthGuardLoginService implements CanActivate{

  	constructor(private commonService: CommonServiceService, private router: Router) { }

  	canActivate() {
		if (this.commonService.getCurrentUser()) {
			this.router.navigateByUrl('/dashboard');
			return false;
		} else {
			return true;
		}
	}

}
