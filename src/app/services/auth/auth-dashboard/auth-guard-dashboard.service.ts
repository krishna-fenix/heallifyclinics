import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CommonServiceService } from '../../common/common-service.service';

@Injectable({
  	providedIn: 'root'
})

/***
* AuthGuardDashboardService
* Authenticate the dashboard routes first and then 
* redirect to appropriate route, only when 
* User has logged in
*/

export class AuthGuardDashboardService implements CanActivate{

  	constructor(private commonService: CommonServiceService, private router: Router) { }

  	canActivate(): boolean {
		// console.log('can activate user state ' + this.mUserService.getCurrentUser());
		if (this.commonService.getCurrentUser())  {
			// console.log('User Instance Found Navigate to Dashboard');
			// this.router.navigateByUrl('dashboard');
			return true;
		} else {
			// console.log(' Navigate to Dashboard, else');
			this.router.navigateByUrl('');
			return false;
		}
	}
}
