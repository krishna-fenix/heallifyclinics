import { TestBed } from '@angular/core/testing';

import { AuthGuardDashboardService } from './auth-guard-dashboard.service';

describe('AuthGuardDashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthGuardDashboardService = TestBed.get(AuthGuardDashboardService);
    expect(service).toBeTruthy();
  });
});
