import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RESTAPI } from '../../restapi';
import { HttpRequestService } from '../http-request.service';


@Injectable({
	providedIn: 'root'
})
export class ProfileService {

	constructor(
		private _http: HttpRequestService
	) { }

	// GET Requests

	getProfileHomePage(): Observable<any> {
		return this._http.get(RESTAPI.DOCTORPROFILEHOMEPAGE);
	}

	profileDetails(): Observable<any> {
		return this._http.get(RESTAPI.GETPROFILEDETAILS);
	}

	getProfileDetailsByDoctorId(doctor_id): Observable<any> {
		return this._http.get(RESTAPI.GETPROFILEDETAILSBYDOCTORID + doctor_id);
	}

	adminProfileDetails(): Observable<any> {
		return this._http.get(RESTAPI.GETADMINDETAILS);
	}
}
