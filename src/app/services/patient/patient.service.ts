import { Injectable } from '@angular/core';
import { RESTAPI } from '../../restapi';
import { Observable } from 'rxjs';
import {HttpRequestService} from '../http-request.service';

@Injectable({
  	providedIn: 'root'
})
export class PatientService {

	constructor(
		private _http: HttpRequestService 
	) { }
	// GET Request
	
	getPatients():Observable<any> {
		return this._http.get(RESTAPI.GETALLPATIENTS);
	}

	getExistingPatient(mobile_number):Observable<any>{
		return this._http.get(RESTAPI.FINDEXISTINGPATIENT + mobile_number);
	}

	findPatients():Observable<any>{
		return this._http.get(RESTAPI.FINDPATIENTS);
	}

	getSharedHealthlogByLogId(log_id):Observable<any>{
		return this._http.get(RESTAPI.GETSHAREDHEALTHLOG + log_id);
	}

	getAllSharedHealthlogByPatientId(patient_id):Observable<any>{
		return this._http.get(RESTAPI.GETALLSHAREDHEALTHLOG + patient_id);
	}

	getHealthRecordsByUserId(user_id):Observable<any>{
		return this._http.get(RESTAPI.GETHEALTHRECORDSBYUSERID + user_id);
	}

	getPatientInfoByPatientId(patient_id):Observable<any>{
		return this._http.get(RESTAPI.GETPATIENTINFOBYPATIENTID + patient_id);
	}

	getPatientPrescriptionsById(user_id):Observable<any>{
		return this._http.get(RESTAPI.GETPATIENTPRESCRIPTION + user_id);
	}

	getPrescriptionsByMainId(main_prescription_id):Observable<any>{
		return this._http.get(RESTAPI.GETPRESCRIPTIONSBYMAINID + main_prescription_id);
	}

	getImmunizationSchedule():Observable<any>{
		return this._http.get(RESTAPI.IMMUNIZATIONSCHEDULE);
	}

	// POST Requests
	updateDataShare():Observable<any>{
		return this._http.post(RESTAPI.UPDATEDATASHARE, {});
	}

	bookmarkPatient():Observable<any>{
		return this._http.post(RESTAPI.BOOKMARKPATIENT, {});
	}

	removeBookmarkByUserId(user_id):Observable<any>{
		return this._http.post(RESTAPI.REMOVEBOOKMARKBYID + user_id, {})
	}

	// Delete Request
	removeHealthRecordsByGalleryItemId(gallery_item_id):Observable<any>{
		return this._http.delete(RESTAPI.DELETEHEALTHRECODEBYGALLERYITEMID + gallery_item_id);
	}
}
