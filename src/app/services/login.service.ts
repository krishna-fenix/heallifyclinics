import { Injectable } from '@angular/core';
import { RESTAPI } from '../restapi';
import { Observable, Observer } from 'rxjs';
import {HttpRequestService} from './http-request.service';



@Injectable({
  providedIn: 'root'
})
export class LoginService {

	constructor(
		private _http: HttpRequestService 
	) { }

	login(loginFormData:any):Observable<any>{
		const body = new FormData();
		for (const key in loginFormData) {
			body.append(key, loginFormData[key]);
		}
		return this._http.post(RESTAPI.LOGIN, body);
	}

	verifyMobile(verifyFormData:any):Observable<any>{
		const body = new FormData();
		for (const key in verifyFormData){
			body.append(key,verifyFormData[key]);
		}
		return this._http.post(RESTAPI.VERIFYMOBILE, body);
	}
	
	sendCode(codeForm:any):Observable<any>{
		const body = new FormData();
		for (const key in codeForm){
			body.append(key,codeForm[key]);
		}
		return this._http.post(RESTAPI.SENDCODE, body);
	}

	resetPassword(resetPasswordForm:any):Observable<any>{
		const body = new FormData();
		for (const key in resetPasswordForm){
			body.append(key,resetPasswordForm[key]);
		}
		return this._http.post(RESTAPI.RESETPASSWORD, body);
	}

	logOut():Observable<any>{
		return this._http.post(RESTAPI.LOGOUT, null);
	}


}
