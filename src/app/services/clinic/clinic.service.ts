import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RESTAPI } from '../../restapi';
import { HttpRequestService } from '../http-request.service';
import { ObserveOnSubscriber } from 'rxjs/internal/operators/observeOn';

@Injectable({
	providedIn: 'root'
})
export class ClinicService {

	constructor(
		private _http: HttpRequestService
	) { }

	// GET Request

	getClinics(): Observable<any> {
		return this._http.get(RESTAPI.GETALLCLINICS);
	}

	getAllHospitals(): Observable<any> {
		return this._http.get(RESTAPI.GETALLHOSPITALS);
	}

	getClinicDetails(data): Observable<any> {
		return this._http.get(RESTAPI.GETCLINICDETAILS + '?city=' + data.city + '&area=' + data.area + '&name=' + data.name);
	}

	getClinicDetailsByDoctorId(doctor_id): Observable<any> {
		return this._http.get(RESTAPI.GETCLINICDETAILSBYDOCTORID + doctor_id);
	}

	getTimeSlotsByDoctorId(doctor_id): Observable<any> {
		return this._http.get(RESTAPI.GETCLINICTIMESLOTS + doctor_id);
	}

	getAllImageForClinicID(clinic_id): Observable<any> {
		return this._http.get(RESTAPI.GETALLIMAGESFORCLINICID + clinic_id);
	}

	getClinicScheduleByClinicId(clinic_id): Observable<any> {
		return this._http.get(RESTAPI.GETCLINICSCHEDULE + clinic_id);
	}

	getClinicLocations(): Observable<any> {
		return this._http.get(RESTAPI.GETLOCATIONAREA);
	}

	// POST Request
	addClinic(formData: any): Observable<any> {
		const body = new FormData();
		for (const key in formData) {
			body.append(key, formData[key]);
		}
		return this._http.post(RESTAPI.ADDCLINIC, body);
	}

	getDoctorReports(body): Observable<any> {
		return this._http.post(RESTAPI.GETDOCTOREPORTS, body);
	}

	editClinicById(clinic_id, body): Observable<any> {
		return this._http.post(RESTAPI.EDITCLINICBYID + clinic_id, body);
	}

	addClinicTimeSlotForClinicID(clinic_id, body): Observable<any> {
		return this._http.post(RESTAPI.ADDCLINICTIMESLOT + clinic_id, body);
	}

	editScheduledClinicForClinicID(clinic_id): Observable<any> {
		return this._http.post(RESTAPI.EDITCLINICSCHEDULE + clinic_id, {});
	}

	addClinicImageByID(clinic_id): Observable<any> {
		return this._http.post(RESTAPI.ADDCLINICIMAGEBYCLINICID + clinic_id, {});
	}

	setDefaultClinicImageByClinicId(clinic_id): Observable<any> {
		return this._http.post(RESTAPI.SETDEFAULTCLINICIMAGEBYCLINICID + clinic_id, {})
	}

	removeClinicImageByID(clinic_id): Observable<any> {
		return this._http.post(RESTAPI.DELETECLINICIMAGEBYCLINICID + clinic_id, {});
	}

	addClinicLogoByClinicId(clinic_id): Observable<any> {
		return this._http.post(RESTAPI.ADDCLINICLOGO + clinic_id, {})
	}

	// DELETE Request
	removeClinicById(id): Observable<any> {
		return this._http.delete(RESTAPI.DELETECLINICBYID + id);
	}

	// Hospital POST

	addHospital(formData: any): Observable<any> {
		const body = new FormData();
		for (const key in formData) {
			body.append(key, formData[key]);
		}
		return this._http.post(RESTAPI.ADDHOSPITAL, body);
	}

	addDoctor(formData: any, url): Observable<any> {
		const body = new FormData();
		for (const key in formData) {
			body.append(key, formData[key]);
		}
		return this._http.post(url, body);
	}

	updateDoctor(formData: any, url): Observable<any> {
		const body = new FormData();
		for (const key in formData) {
			body.append(key, formData[key]);
		}
		return this._http.post(url, body);
	}

	// Get

	getDoctors(url) {
		return this._http.get(url);
	}


}
