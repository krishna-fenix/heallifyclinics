import { TestBed } from '@angular/core/testing';

import { StoreNotificationDataService } from './store-notification-data.service';

describe('StoreNotificationDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StoreNotificationDataService = TestBed.get(StoreNotificationDataService);
    expect(service).toBeTruthy();
  });
});
