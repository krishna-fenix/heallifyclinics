import { Injectable } from '@angular/core';

@Injectable({
  	providedIn: 'root'
})
export class StoreNotificationDataService {

  	constructor() { }

	notification_data:any;

	set storeNotificationData(data:any){
		console.log(data);
		this.notification_data = data;
	}

	get storeNotificationData():any{
		console.log(this.notification_data);
		return this.notification_data;
	}

}
