import { Injectable } from '@angular/core';
import { RESTAPI } from '../../../restapi';
import { Observable } from 'rxjs';
import {HttpRequestService} from '../../http-request.service';

@Injectable({
  	providedIn: 'root'
})
export class RegisterdoctorService {

	constructor(
		private _http: HttpRequestService 
	) { }

	registerDoctor(newRegister: any):Observable<any>{
		const body = new FormData();
		for (const key in newRegister) {
			body.append(key, newRegister[key]);
		}
		return this._http.post(RESTAPI.REGISTERDOCTOR, body);
	}
  

}
