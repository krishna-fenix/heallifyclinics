import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestService } from '../http-request.service'
import { RESTAPI } from 'src/app/restapi';

@Injectable({
  	providedIn: 'root'
})
export class DoctorCommonService {

  constructor(private _http:HttpRequestService) { }

  	// GET Requests
	getReviews():Observable<any>{
		return this._http.get(RESTAPI.GETREVIEWS);
	}

	getAllDrugs():Observable<any>{
		return this._http.get(RESTAPI.GETALLDRUGS);
	}

	getDoctorFaceSheet(appointment_id):Observable<any>{
		return this._http.get(RESTAPI.GETDOCTORFACESHEET + appointment_id);
	}

	getDoctorAutoComplete():Observable<any>{
		return this._http.get(RESTAPI.GETDOCTORAUTOCOMPLETE);
	}

	getAllergiesAutocomplete(allergie_name):Observable<any>{
		return this._http.get(RESTAPI.GETALLERGIESAUTOCOMPLETE + '?name=' + allergie_name);
	}

	getConditionsAutocomplete(condition_name):Observable<any>{
		return this._http.get(RESTAPI.GETCONDITIONSAUTOCOMPLETE + '?name=' + condition_name);
	}

	getAutocompleteForDigitalPrescription():Observable<any>{
		return this._http.get(RESTAPI.AUTOCOMPLETEDIGITALPRESCRIPTION);
	}

	// POST Requests
	markHealthrecordSeenByRefId(ref_id):Observable<any>{
		return this._http.post(RESTAPI.MARKHEALTHLOGSEEN + ref_id, {});
	}

	markReviewReadUnread(body):Observable<any>{
		return this._http.post(RESTAPI.MARKREVIEWREADUNREAD, body);
	}

	createNewPrescription(body):Observable<any>{
		return this._http.post(RESTAPI.CREATENEWPRESCRIPTION, body);
	}

	addPrescriptionDetails(main_prescription_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDPRESCRIPTIONDETAILS + main_prescription_id, body);
	}

	updatePrescription(main_prescription_id, prescription_id, body):Observable<any>{
		return this._http.post(RESTAPI.UPDATEPRESCRIPTIONBYID + main_prescription_id + '/' + prescription_id, body);
	}

	markAsDone(user_id, vaccine_id):Observable<any>{
		return this._http.post(RESTAPI.MARKASDONE + user_id + '/' + vaccine_id, {});
	}

	notesToPatient(appointment_id):Observable<any>{
		return this._http.post(RESTAPI.NOTESTOPATIENT + appointment_id, {})
	}

	callBack(appointment_id):Observable<any>{
		return this._http.post(RESTAPI.CALLBACK + appointment_id, {});
	}

	callBackReattempt(appointment_id):Observable<any>{
		return this._http.post(RESTAPI.CALLBACKREATTEMPT + appointment_id, {});
	}

	addConditions(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDCONDITIONS + appointment_id, body);
	}

	addAllergies(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDALLERGIES + appointment_id, body);
	}

	addDiagonosis(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDDIAGONOSIS + appointment_id, body);
	}

	addPhysicalExamination(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDPHYSICALEXAMINATION + appointment_id, body);
	}

	addTestNotes(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDDIAGONOSIS + appointment_id, body);
	}

	addLogs(appointment_id, body):Observable<any>{
		return this._http.post(RESTAPI.ADDLOGS + appointment_id, body);
	}
	// PUT Request
	updateAssignedPrescription(main_prescription_id):Observable<any>{
		return this._http.put(RESTAPI.UPDATEPRESCRIPTION + main_prescription_id, {})
	}

	addRole(role_name, parent_role_id):Observable<any>{
		return this._http.post(RESTAPI.ADDROLE + role_name + '/' + parent_role_id, {});
	}

	// DELETE Requests

	removePrescription(main_prescription_id, prescription_id):Observable<any>{
		return this._http.delete(RESTAPI.DELETEPRESCRIPTIONBYID + main_prescription_id + '/' + prescription_id);
	}

	removeFullPrescription(main_prescription_id):Observable<any>{
		return this._http.delete(RESTAPI.DELETEFULLPRESCRIPTIONBYID + main_prescription_id);
	}

	createZoomMeeting(body):Observable<any>{
		const option = {
			headers:{
				access_token:"eyJhY2Nlc3NfdG9rZW4iOiJhZDMxNzkyYWQ1NTA1ZWEzNmE1N2JlY2RiYTAyM2EwZGVmYzEzNzJiIiwiYXV0aF9pZCI6IjI4IiwiY3JlYXRlZF9hdCI6IjIwMjAtMDEtMTcgMTk6MzE6NTQifQ=="
				// window.localStorage.getItem('my-app.access_token')
			}
		}
		return this._http.post(RESTAPI.STARTZOOMEETING, body, option);
	}

	endZoomMeeting(body):Observable<any>{
		const option = {
			headers:{
				access_token:"eyJhY2Nlc3NfdG9rZW4iOiJhZDMxNzkyYWQ1NTA1ZWEzNmE1N2JlY2RiYTAyM2EwZGVmYzEzNzJiIiwiYXV0aF9pZCI6IjI4IiwiY3JlYXRlZF9hdCI6IjIwMjAtMDEtMTcgMTk6MzE6NTQifQ=="
				// window.localStorage.getItem('my-app.access_token')
			}
		}
		return this._http.post(RESTAPI.ENDZOOMMEETING, body, option);
	}

}
