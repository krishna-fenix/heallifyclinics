import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { ToastrDisplayService } from 'src/app/services/toastr.service';
import { User } from '../../models/user';
import { LoaderState } from '../../models/loader-state';

@Injectable({
  	providedIn: 'root'
})

export class CommonServiceService {

	mUser: User;

	private loaderSubject = new Subject<LoaderState>();
	loaderState = this.loaderSubject.asObservable();

	private mSubject = new Subject<string>();
	private mSubject2 = new Subject<string>();
	private mSubject3 = new Subject<string>();
	
	mTitle = this.mSubject.asObservable();
	mBreadCrumb = this.mSubject2.asObservable();
	mButton = this.mSubject3.asObservable();

  	constructor(
		private mActivatedRoute: ActivatedRoute,
		private router: Router,
		private toasterservice: ToastrDisplayService,
		private localStorage: LocalStorageService
	) { 
		this.mUser = this.localStorage.get('user') as User;
	}

	log(message: string) {
		console.log(message);
	}

	// user-service
	getCurrentUser(): User {
		return this.mUser;
	}

	setCurrentUser(user: User) {
		this.mUser = user;
	}

	updateUserInstance(first_name: string, last_name: string) {
		this.getCurrentUser().first_name = first_name;
		this.getCurrentUser().last_name = last_name;
		this.localStorage.set('user', this.getCurrentUser());
	}

	setUserInstance(resp) {
		this.localStorage.set('user', resp.data);
		let user: User;
		user = resp.data;
		this.setCurrentUser(user);
	}

	// 

	// loader-service
	showLoader() {
		this.loaderSubject.next(<LoaderState>{show: true});
	}

	hideLoader() {
		this.loaderSubject.next(<LoaderState>{show: false});
	}
	//

	// breadcrumb-service

	setBreadCrumbTitle(title: string) {
		this.mSubject.next(title);
	}

	addBreadCrumbTitle(title: string) {
		this.mSubject2.next(title);
	}

	setBreadCrumbButton(title: string) {
		this.mSubject3.next(title);
	}
	//
	
	getFormData(data: FormData): FormData {
		data.append('access_token', this.getCurrentUser().token);
		return data;
	}

	saveToken(token: string) {
		this.localStorage.set('access_token', token);
	}

	isDoctor(): Boolean {
		if (this.getCurrentUser() != null) {
			return this.getCurrentUser().type === '2';
		}
	}

	isStaff(): Boolean {
		if (this.getCurrentUser() != null) {
			return this.getCurrentUser().type === '3';
		}
	}

	// Toaster-services
	showSuccess(title: string, message?: string) {
		console.log("in toaster")
		this.toasterservice.showSuccess(title, message);
	}

	showError(title: string, message?: string) {
		this.toasterservice.showError(title, message);
	}

	showInfo(title: string, message?: string) {
		this.toasterservice.showInfo(title, message);
	}

	showWarning(title: string, message?: string){
		this.toasterservice.showWarning(title, message);
	}
	// 

	navigateByUrl(url: string, data?) {
		if (data != null) {
			console.log('data = ' + JSON.stringify(data));
			this.router.navigateByUrl(url, data);
		} else {
			this.router.navigateByUrl(url);
		}
	}

	navigate(url: string[], data?: any) {
		if (data != null) {
			console.log('data = ' + JSON.stringify(data));
			this.router.navigate(url, data);
		} else {
			this.router.navigate(url);
		}
	}
	
}
