import { TestBed } from '@angular/core/testing';

import { DoctorCommonService } from './doctor-common.service';

describe('DoctorCommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DoctorCommonService = TestBed.get(DoctorCommonService);
    expect(service).toBeTruthy();
  });
});
