import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavDraweroperatorService {
  mValue = false;
  constructor() { }

  toggle() {
    this.mValue = !this.mValue;
    console.log(this.mValue);
  }

  getState() {
    return this.mValue;
  }
}
