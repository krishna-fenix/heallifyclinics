import { Injectable } from '@angular/core';
import { MatDrawer } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class DrawerService {

  mDrawer: MatDrawer;


  public toggle(delay?: number) {
    setTimeout(
      () => {
        this.mDrawer.toggle();
      }, delay ? delay : 0);
  }

  close() {
    this.mDrawer.close();
    setTimeout(
      () => {
        this.mDrawer.open();
      }, 300
    );
  }

  setDrawer(drawer: MatDrawer) {
    this.mDrawer = drawer;
  }

  open() {
    this.mDrawer.open();
  }
}
