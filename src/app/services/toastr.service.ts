import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastrDisplayService {

  constructor(private mToastr: ToastrService ) {}

  public openSnackBar(message: string, action: string) {
    // this.mSnackBar.open(message, action, {
    //   duration: 2000,
    // });
  }

  public showSuccess(title: string, message?: string) {
    //  this.checkVcr();
       this.mToastr.success(title, message);
    }

  public showError(title: string , message?: string) {
    //this.checkVcr();
    this.mToastr.error(title, message);
  }

  public showWarning(title: string, message: string) {
    // this.checkVcr();
     this.mToastr.warning(title, message);
  }
  
  public showInfo(title: string, message: string) {
    // this.checkVcr();
    this.mToastr.info(title, message);
  }
}
