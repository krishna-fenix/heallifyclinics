import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs'
import { RESTAPI } from '../../restapi';
import { Observable } from 'rxjs/Observable';
import { CommonServiceService } from '../common/common-service.service';
import { HttpRequestService } from '../http-request.service';
import { debug } from 'util';
import { MatDialog } from '@angular/material';
import { CallDialogComponent } from 'src/app/dashboard/video-call-dialog/call-dialog/call-dialog.component';
import { StoreNotificationDataService } from '../store-notification/store-notification-data.service';
@Injectable({
  	providedIn: 'root'
})
export class FirebaseMessagingService {

	currentMessage = new BehaviorSubject(null);
	permission:any = '';
	myAudio:any;
	constructor(
		private _http:HttpRequestService,
		public dialog: MatDialog,
		private storeNotificationDataService:StoreNotificationDataService,
		private commonService:CommonServiceService,
		private angularFireDB: AngularFireDatabase,
		private angularFireAuth: AngularFireAuth,
		private angularFireMessaging: AngularFireMessaging) {
		this.angularFireMessaging.messaging.subscribe(
			(_messaging) => {
				_messaging.onMessage = _messaging.onMessage.bind(_messaging);
				_messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
			}
		)
		this.permission = this.isSupported() ? 'default' : 'denied';
		if('Notification' in window){
			var self = this;
			Notification.requestPermission(function (status) {
				self.permission = status;
			});
		}
		this.myAudio = new Audio();
		this.myAudio.src = '../../assets/audio/Phonering.wav';
		this.myAudio.load();
		// this.myAudio.play();
	}

	public isSupported(): boolean {
		return 'Notification' in window;
	}

	/**
	 * update token in firebase database
	 * 
	 * @param userId userId as a key 
	 * @param token token as a value
	 */
	updateToken(userId, token) {
		// we can change this function to request our backend service
		this.angularFireAuth.authState.pipe(take(1)).subscribe(
			() => {
				const data = {};
				data[userId] = token
				this.angularFireDB.object('fcmTokens/').update(data)
			}
		)
	}

	/**
	 * request permission for notification from firebase cloud messaging
	 * 
	 * 
	 * @param userId userId
	 */
	requestPermission(userId) {
		this.angularFireMessaging.requestToken.subscribe(
			(token) => {
				console.log(token);
				this.addGCMId(token).subscribe(
					(response) => {
						console.log("addGCM_Response",response);
						this.updateGCMId(token).subscribe((response) => {
							console.log("updateGCM_Response",response);
						},(error)=>{
							console.log("error from updateGCM",error);
						})
					},
					(error) => {
						console.log("addGCM_error",error);
					}
				)
			},
			(err) => {
				console.error('Unable to get permission to notify.', err);
			}
		);
	}

	/**
	 * hook method when new notification received in foreground
	 */
	receiveMessage() {
		
		this.angularFireMessaging.messages.subscribe(
			(payload) => {
				console.log("new message received. ", payload);
				this.currentMessage.next(payload);
				console.log(this.currentMessage);
				this.generateNotification(payload)
			}
		)
	}

	addGCMId(gcm_id){
		var formData = new FormData;
		formData.append('gcm_id', gcm_id);
		var user = this.commonService.getCurrentUser();
		formData.append('device_id', "WEB_" + user.first_name + user.last_name + user.email);
		return this._http.post(RESTAPI.ADDGCMID, formData);
	}

	updateGCMId(gcm_id){
		var formData = new FormData;
		formData.append('gcm_id', gcm_id);
		var user = this.commonService.getCurrentUser();
		formData.append('device_id', "WEB_" + user.first_name + user.last_name + user.email);
		return this._http.post(RESTAPI.UPDATEGCMID, formData);
	}

	openAddCallingDialog(data): void {
		const dialogRef = this.dialog.open(CallDialogComponent, {
			width: '500px',
			height: 'fit-content',
			data:data
		});
		dialogRef.afterClosed().subscribe(result => {
			console.log(result);
			if(result == 'declined')
				this.commonService.showInfo("The call has declined!!!");
		})
	}

	generateNotification(source:any){
		const self = this;
		console.log(source);
		const options = {
			type: 'VIDEO CALL',
			body: source.data.notificationtitle,
			icon: '../../assets/images/favicon.ico',
			silent: false,
			sound: '../../assets/audio/Phonering.wav'
		};
		const notify = self.create(source, options).subscribe();
		var dialogData = {
			join_url: source.data.key1,
			appointment_id: source.data.key2,
			title: source.data.notificationtitle,
			type: source.data.notificationtype
		}
		this.storeNotificationDataService.storeNotificationData = dialogData;
		this.openAddCallingDialog(dialogData);
	}

	create(payload: any, options?: any): any {
		// const notification = new Notification('Hi there', options);
		this.myAudio.play();
		const self = this;
		return new Observable(function (obs) {
			const title = options.type;
			if (!('Notification' in window)) {
				console.log('Notifications are not available in this environment');
				obs.complete();
			}

			if (self.permission !== 'granted') {
				console.log('The user has not granted you permission to send push notifications');
				obs.complete();
			}

			const _notify = new Notification(title, options);
			_notify.onshow = function (e) {
				
				return obs.next({
					notification: _notify,
					event: e
				});
			};

			_notify.onclick = function (e) {
				if (options) {
					
				}
			};

			_notify.onerror = function (e) {
				return obs.error({
					notification: _notify,
					event: e
				});
			};

			_notify.onclose = function () {
				return obs.complete();
			};
		});
	}

	sendNotification(body){
		const option = {
			headers:{
				access_token: window.localStorage.getItem('my-app.access_token')
				// "eyJhY2Nlc3NfdG9rZW4iOiJhZDMxNzkyYWQ1NTA1ZWEzNmE1N2JlY2RiYTAyM2EwZGVmYzEzNzJiIiwiYXV0aF9pZCI6IjI4IiwiY3JlYXRlZF9hdCI6IjIwMjAtMDEtMTcgMTk6MzE6NTQifQ=="
			}
		}
        return this._http.post(RESTAPI.SENDNOTIFICATIONS, body, option);
    }

}
