import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { CommonServiceService } from 'src/app/services/common/common-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
	profile: any;

	constructor(
		private profileService:ProfileService,
		private commonService:CommonServiceService,
		private route:ActivatedRoute
	) { }

	ngOnInit() {
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			let hUnicode = this.route.parent.params['value'].hospital_unicode;
			let dUnicode = this.route.parent.params['value'].doctor_unicode;
			if( (hUnicode == window.localStorage.getItem('hosp_unicode')) && (dUnicode == window.localStorage.getItem('doc_unicode')) ){
				// this.is_multitenantActive = true;
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this.commonService.navigateByUrl('');
			}
		}
		this.getProfileDetails();
	}

	getProfileDetails() {
		this.profileService.profileDetails().subscribe(
			(data) => {
				console.log(data);
				if (data.success === '1') {
					console.log("in success");
					this.profile = data.data;
				} else {
					this.commonService.showError(data.user_friendly_message)
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wront!!!");
			}
		)
	}


}
