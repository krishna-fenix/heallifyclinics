import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonServiceService } from 'src/app/services/common/common-service.service';

@Component({
    selector: 'app-setting-sidenav',
    templateUrl: './setting-sidenav.component.html',
    styleUrls: ['./setting-sidenav.component.scss']
})
export class SettingSidenavComponent implements OnInit {

    mDrawerShrink: any
    is_multitenantActive: boolean = false;
	url:string = '/hospital/';
    settingLinks: { route: string; title: string; show: boolean; }[];
    constructor(
        private router: Router,
        private route:ActivatedRoute,
        private commonService:CommonServiceService
    ) { }

    ngOnInit() {
        if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			let hUnicode = this.route.parent.params['value'].hospital_unicode;
			let dUnicode = this.route.parent.params['value'].doctor_unicode;			
			if( (hUnicode == window.localStorage.getItem('hosp_unicode')) && (dUnicode == window.localStorage.getItem('doc_unicode')) ){
                this.is_multitenantActive = true;
                this.url += hUnicode + "/" + dUnicode; 
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this.commonService.navigateByUrl('');
			}
        }
        this.getSideBar();
    }
    getSideBar(){
        this.settingLinks = [
            { route: '/dashboard/profilepage', title: 'Profile', show: !this.is_multitenantActive  },
            { route: '/dashboard/notification', title: 'Notification', show: !this.is_multitenantActive  },
            { route: '/dashboard/role', title: 'Roles', show: !this.is_multitenantActive  },
            { route: this.url + '/profilepage', title: 'Profile', show: this.is_multitenantActive  },
            { route: this.url + '/notification', title: 'Notification', show: this.is_multitenantActive  },
            { route: this.url + '/role', title: 'Roles', show: this.is_multitenantActive  },
            // { route: '/dashboard/staff', title: 'Staff' },
            // { route: '/dashboard/subscription', title: 'Subscription' },
            // { route: '/dashboard/subscription', title: 'Invoices' },
            // { route: '/dashboard/subscription', title: 'Billing Information' },
        ];
    }
}
