import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-fields',
  templateUrl: './add-fields.component.html',
  styleUrls: ['./add-fields.component.scss']
})
export class AddFieldsComponent {

  // @Input() myForm: FormGroup;
  @Input('group')
    public addDiagnosisForm: FormGroup;
    
}
