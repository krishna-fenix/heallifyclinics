import { Component, OnInit } from '@angular/core';
import { AddDrugsComponent } from '../add-drugs/add-drugs.component';
import { MatDialog } from '@angular/material';
import {PatientService} from '../../../services/patient/patient.service';
import {CommonServiceService} from '../../../services/common/common-service.service';

export interface PrescriptionListing {
	drugname: string;
	strength: string;
	frequency: string;
	duration: string;
	totalintake: string;
}

const prescription_data: PrescriptionListing[] = [
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' }
];

@Component({
	selector: 'app-prescription',
	templateUrl: './prescription.component.html',
	styleUrls: ['./prescription.component.scss']
})
export class PrescriptionComponent implements OnInit {

	constructor(
		public dialog: MatDialog,
		private commonService:CommonServiceService,
		private patientService:PatientService
	) { }

	ngOnInit() {
		this.loadPrescriptions()
	}

	openAddDrugsDialog(): void {
		const dialogRef = this.dialog.open(AddDrugsComponent, {
			width: '896px',
			height: 'fit-content',
		});
	}

	displayedColumns: string[] = ['drugname', 'strength', 'frequency', 'duration', 'totalintake'];
	dataSource = prescription_data;

	loadPrescriptions(){
		let patient_id = 16;
		this.patientService.getPatientPrescriptionsById(patient_id).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success === '1'){
					console.log("getPatientPrescriptionsById()" + response.data);
					this.commonService.showSuccess(response.user_friendly_message);
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	getPrescriptionsByMainID(main_prescription_id){
		this.patientService.getPrescriptionsByMainId(main_prescription_id).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success === '1'){
					console.log("getPrescriptionsByMainId()" + response.data);
					this.commonService.showSuccess(response.user_friendly_message);
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

}
