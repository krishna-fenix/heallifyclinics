import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EHRSidebarComponent } from './ehr-sidebar.component';

describe('EHRSidebarComponent', () => {
  let component: EHRSidebarComponent;
  let fixture: ComponentFixture<EHRSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EHRSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EHRSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
