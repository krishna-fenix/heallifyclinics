import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-ehr-sidebar',
	templateUrl: './ehr-sidebar.component.html',
	styleUrls: ['./ehr-sidebar.component.scss']
})
export class EHRSidebarComponent implements OnInit {
	
	mDrawerShrink:any;

	constructor(private router: Router) { }

	ngOnInit() {
	}

	ehrLinks = [
		{ route: '/dashboard/medicalhistory', title: 'Medical History' },
		{ route: '/dashboard/healthvital', title: 'Health Vital' },
		{ route: '/dashboard/allergies', title: 'Allergies' },
		{ route: '/dashboard/conditions', title: 'Conditions' },
		{ route: '/dashboard/diagnosis', title: 'Diagnosis' },
		{ route: '/dashboard/prescription', title: 'Prescription' },
		{ route: '/dashboard/labtests', title: 'Order Lab Tests' }
  	];

}
