import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EhrActionBarComponent } from './ehr-action-bar.component';

describe('EhrActionBarComponent', () => {
  let component: EhrActionBarComponent;
  let fixture: ComponentFixture<EhrActionBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EhrActionBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EhrActionBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
