import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-ehr-action-bar',
	templateUrl: './ehr-action-bar.component.html',
	styleUrls: ['./ehr-action-bar.component.scss']
})
export class EhrActionBarComponent implements OnInit {

	constructor(private router:Router) { }

	ngOnInit() {
	}

	goToAppointmentSummary(){
		this.router.navigateByUrl('/dashboard/appointmentsummary')
	}

}
