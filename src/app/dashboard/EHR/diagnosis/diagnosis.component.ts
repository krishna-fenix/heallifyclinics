import { Component, OnInit, Input } from '@angular/core';
import { ArrayType } from '@angular/compiler';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { DynamicGrid } from 'src/app/models/grid.model';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { DoctorCommonService } from 'src/app/services/common/doctor-common.service';

@Component({
    selector: 'app-diagnosis',
    templateUrl: './diagnosis.component.html',
    styleUrls: ['./diagnosis.component.scss']
})
export class DiagnosisComponent implements OnInit {


    constructor(
        private doctorCommonService:DoctorCommonService,
		private commonService:CommonServiceService
    ) { }

    dynamicArray: Array<DynamicGrid> = [];
    newDynamic: any = {};
    ngOnInit(): void {
        this.newDynamic = { diagnosis: ""};
        this.dynamicArray.push(this.newDynamic);
    }

    addRow() {
        this.newDynamic = { diagnosis: ""}; // this line is added so the every field will be different. if line is removed the controls added will display same values
        this.dynamicArray.push(this.newDynamic);
        return true;
    }

    deleteRow(index) {
        this.dynamicArray.splice(index, 1);
        return true;
    }

    addDiagonosis(){
        let appointment_id = 1;
        let formData = new FormData;
		this.doctorCommonService.addDiagonosis(appointment_id, formData).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

}
