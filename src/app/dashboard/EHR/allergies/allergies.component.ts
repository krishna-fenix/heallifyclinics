import { Component, OnInit } from '@angular/core';
import { DynamicGrid } from 'src/app/models/grid.model';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { DoctorCommonService } from 'src/app/services/common/doctor-common.service';

@Component({
	selector: 'app-allergies',
	templateUrl: './allergies.component.html',
	styleUrls: ['./allergies.component.scss']
})
export class AllergiesComponent implements OnInit {

	constructor(
		private doctorCommonService:DoctorCommonService,
		private commonService:CommonServiceService
	) { }

	dynamicArray: Array<DynamicGrid> = [];
	newDynamic: any = {};
	allergies:any;
	filteredAllergies:any;

	ngOnInit(): void {
		this.newDynamic = { type: "", description: "", severity: "" };
		this.dynamicArray.push(this.newDynamic);
	}

	addRow() {
		this.newDynamic = { type: "", description: "", severity: "" }; // this line is added so the every field will be different. if line is removed the controls added will display same values
		this.dynamicArray.push(this.newDynamic);
		return true;
	}

	deleteRow(index) {
		this.dynamicArray.splice(index, 1);
		return true;
	}

	displayFn(value): string {
		return value && value.name ? value.name : '';
	}

	onSelection(value){
		console.log(value);
	}

	getAllergiesAutocomplete(allergies_name){
		this.doctorCommonService.getAllergiesAutocomplete(allergies_name).subscribe(
			(response:any) => {
				console.log(response.data);
				this.allergies = response.data;
				const filterValue = allergies_name.toLowerCase();
				this.filteredAllergies = this.allergies.filter(option => option.name != null && option.name.toLowerCase().includes(filterValue));
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	addAllergies(){
		let appointment_id = 1;
		let formData = new FormData;
		this.doctorCommonService.addAllergies(appointment_id, formData).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

}
