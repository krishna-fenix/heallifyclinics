import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { DoctorCommonService } from 'src/app/services/common/doctor-common.service';
import { MatDialog } from '@angular/material';
import {SetFollowupAppointmentComponent} from '../set-followup-appoitment/set-followup-appointment/set-followup-appointment.component';

export interface PrescriptionListing {
	drugname: string;
	strength: string;
	frequency: string;
	duration: string;
	totalintake: string;
}

const prescription_data: PrescriptionListing[] = [
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' },
	{ drugname: '1', strength: 'Heallify', frequency: '1234567890', duration: 'hadapsar', totalintake: 'active' }
];

@Component({
	selector: 'app-appointment-summary',
	templateUrl: './appointment-summary.component.html',
	styleUrls: ['./appointment-summary.component.scss']
})
export class AppointmentSummaryComponent implements OnInit {

	constructor(
		private router:Router,
		public dialog: MatDialog,
		private doctorCommonService:DoctorCommonService,
		private commonService:CommonServiceService
	) { }

	ngOnInit() {
	}

	openFollowUpDialog(): void {
		const dialogRef = this.dialog.open(SetFollowupAppointmentComponent, {
			width: '896px',
			height: 'fit-content',
		});
	}

	navigateToPrescription(){
		this.router.navigateByUrl('/dashboard/prescription')
	}

	navigateToDiagnosis(){
		this.router.navigateByUrl('/dashboard/diagnosis')
	}

	navigateToAllergies(){
		this.router.navigateByUrl('/dashboard/allergies')
	}
	
	navigateToConditions(){
		this.router.navigateByUrl('/dashboard/conditions')
	}

	navigateToLabTests(){
		this.router.navigateByUrl('/dashboard/labtests')
	}

	displayedColumns: string[] = ['drugname', 'strength', 'frequency', 'duration', 'totalintake'];
	dataSource = prescription_data;

	getFaceSheet(appointment_id){
		this.doctorCommonService.getDoctorFaceSheet(appointment_id).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	addLogs(){
		let appointment_id = 1;
		let formData =  new FormData;
		this.doctorCommonService.addLogs(appointment_id, formData).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}


}
