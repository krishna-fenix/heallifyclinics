import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { DoctorCommonService } from 'src/app/services/common/doctor-common.service';

@Component({
	selector: 'app-health-vitals',
	templateUrl: './health-vitals.component.html',
	styleUrls: ['./health-vitals.component.scss']
})
export class HealthVitalsComponent implements OnInit {

	constructor(
		private doctorCommonService:DoctorCommonService,
		private commonService:CommonServiceService
	) { }

	ngOnInit() {
		// this.getFaceSheet(721);
	}

	getFaceSheet(appointment_id){
		this.doctorCommonService.getDoctorFaceSheet(appointment_id).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	addHealthVitals(){
		let appointment_id = 1;
		let formData = new FormData;
		this.doctorCommonService.addPhysicalExamination(appointment_id, formData).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

}
