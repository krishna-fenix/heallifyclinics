import { Component, OnInit } from '@angular/core';
import { DynamicGrid } from 'src/app/models/grid.model';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { DoctorCommonService } from 'src/app/services/common/doctor-common.service';

@Component({
	selector: 'app-conditions',
	templateUrl: './conditions.component.html',
	styleUrls: ['./conditions.component.scss']
})
export class ConditionsComponent implements OnInit {

    constructor(
		private doctorCommonService:DoctorCommonService,
		private commonService:CommonServiceService
	) { }

    dynamicArray: Array<DynamicGrid> = [];
	newDynamic: any = {};
	
	conditions:any;
	filteredConditions:any;

    ngOnInit(): void {
		this.newDynamic = { condition: "", notes: ""};
		this.dynamicArray.push(this.newDynamic);
    }

    addRow() {
		this.newDynamic = { condition: "", notes: ""}; // this line is added so the every field will be different. if line is removed the controls added will display same values
		this.dynamicArray.push(this.newDynamic);
		return true;
    }

    deleteRow(index) {
        this.dynamicArray.splice(index, 1);
        return true;
	}

	displayFn(value): string {
		return value && value.name ? value.name : '';
	}

	onSelection(value){
		console.log(value);
	}
	
	getConditionsAutocomplete(condition_name){
		this.doctorCommonService.getConditionsAutocomplete(condition_name).subscribe(
			(response:any) => {
				console.log(response);
				this.conditions = response.data;
				const filterValue = condition_name.toLowerCase();
				this.filteredConditions = this.conditions.filter(option => option.name != null && option.name.toLowerCase().includes(filterValue));
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	addConditions(){
		let appointment_id = 1;
		let formData = new FormData;
		this.doctorCommonService.addConditions(appointment_id, formData).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

}
