import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { DoctorCommonService } from 'src/app/services/common/doctor-common.service';

@Component({
	selector: 'app-add-drugs',
	templateUrl: './add-drugs.component.html',
	styleUrls: ['./add-drugs.component.scss']
})
export class AddDrugsComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<AddDrugsComponent>,
		private doctorCommonService:DoctorCommonService,
		private commonService:CommonServiceService
	) { }

	frequencies:any;
	dosageUnit:any;
	durations:string[] = ['Days', 'Weeks', 'Months'];

	ngOnInit() {
		this.getAutocompleteDigitalPrescription();
	}

	onClose() {
		this.dialogRef.close();
	}

	getAutocompleteDigitalPrescription(){
		this.doctorCommonService.getAutocompleteForDigitalPrescription().subscribe(
			(response:any) => {
				console.log(response);
				this.frequencies = response.data.frequency;
				this.dosageUnit = response.data["dosage units"];
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	createNewPrescription(){
		var formData = new FormData;
		this.doctorCommonService.createNewPrescription(formData).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	getDrugAutoComplete(){
		this.doctorCommonService.getAllDrugs().subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	addPrescriptionDetails(){
		var main_prescription_id = 1;
		let formData = new FormData;
		this.doctorCommonService.addPrescriptionDetails(main_prescription_id, formData).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	updatePrescriptionById(){
		var main_prescription_id = 1, prescription_id = 2;
		let formData = new FormData;
		this.doctorCommonService.updatePrescription(main_prescription_id, prescription_id, formData).subscribe(
			(response:any) => {
				console.log(response);
				this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

}
