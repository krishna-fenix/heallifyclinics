import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetFollowupAppointmentComponent } from './set-followup-appointment.component';

describe('SetFollowupAppointmentComponent', () => {
  let component: SetFollowupAppointmentComponent;
  let fixture: ComponentFixture<SetFollowupAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetFollowupAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetFollowupAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
