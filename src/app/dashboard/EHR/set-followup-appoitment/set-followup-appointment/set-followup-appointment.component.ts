import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { CommonServiceService } from "../../../../services/common/common-service.service";
import { DoctorCommonService } from 'src/app/services/common/doctor-common.service';

@Component({
  selector: 'app-set-followup-appointment',
  templateUrl: './set-followup-appointment.component.html',
  styleUrls: ['./set-followup-appointment.component.scss']
})
export class SetFollowupAppointmentComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<SetFollowupAppointmentComponent>,
		private doctorCommonService:DoctorCommonService,
		private commonService:CommonServiceService
	) { }

	ngOnInit() {
	}

 	onClose() {
		this.dialogRef.close();
	}

}
