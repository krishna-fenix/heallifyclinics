import { NgModule } from '@angular/core';
import { CommonMaterialModule } from '../material-common/common-material.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ActionToolbarComponent } from '../common-ui-components/action-toolbar/action-toolbar.component';
import { SideNavigationComponent } from '../common-ui-components/side-navigation/side-navigation.component';
import { AppointmentListComponent } from './appointment/appointment-list/appointment-list.component';
import { DeclineDialogComponent } from './appointment/decline-dialog/decline-dialog.component';
import { RescheduleDialogComponent } from './appointment/reschedule-dialog/reschedule-dialog.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatSelectModule, MatInputModule } from '@angular/material';
import { MatCardModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { MedicalHistoryComponent } from './EHR/medical-history/medical-history.component';
import { EHRSidebarComponent } from './EHR/ehr-sidebar/ehr-sidebar.component';
import { HealthVitalsComponent } from './EHR/health-vitals/health-vitals.component';
import { DashboardflexComponent } from './dashboardflex/dashboardflex.component';
import { DashboardComponent } from './dashboard.component';
import { PrescriptionComponent } from './EHR/prescription/prescription.component';
import { AddDrugsComponent } from './EHR/add-drugs/add-drugs.component';
import { DiagnosisComponent } from './EHR/diagnosis/diagnosis.component';
import { AddFieldsComponent } from './EHR/add-fields/add-fields.component';
import { AllergiesComponent } from './EHR/allergies/allergies.component';
import { ConditionsComponent } from './EHR/conditions/conditions.component';
import { OrderLabTestComponent } from './EHR/order-lab-test/order-lab-test.component';
import { AppointmentSummaryComponent } from './EHR/appointment-summary/appointment-summary.component';
import { EhrActionBarComponent } from './EHR/ehr-action-bar/ehr-action-bar.component';
import { NavdrawerComponent } from '../common-ui-components/navdrawer/navdrawer.component';
import { SetFollowupAppointmentComponent } from './EHR/set-followup-appoitment/set-followup-appointment/set-followup-appointment.component';
import { VideoConsultingComponent } from './video-consulting/video-consulting.component';
import { MatTabsModule } from '@angular/material/tabs';
import { CallDialogComponent } from './video-call-dialog/call-dialog/call-dialog.component';
import { ClinicDashboardComponent } from './clinic-dashboard/clinic-dashboard.component';
import { TimepickerModule } from 'ngx-bootstrap';
import { GoogleChartComponent } from 'angular-google-charts';
import { AddEducationComponent } from './configure-profile/add-education/add-education.component';
import { AddLabReportsComponent } from './patient/add-lab-reports/add-lab-reports.component';
import { ProfilePageComponent } from './configure-profile/profile-page/profile-page.component';
import { ConfigureFormComponent } from './configure-profile/configure-form/configure-form.component';
import { AddPatientComponent } from './patient/add-patient/add-patient.component';
import { NotificationComponent } from './settings/notification/notification.component';
import { PatientAppointmentHistoryComponent } from './patient/patient-appointment-history/patient-appointment-history.component';
import { PatientHistorySidenavComponent } from './patient/patient-history-sidenav/patient-history-sidenav.component';
import { PatientLabReportsComponent } from './patient/patient-lab-reports/patient-lab-reports.component';
import { PatientListComponent } from './patient/patient-list/patient-list.component';
import { PatientMedicalHistoryComponent } from './patient/patient-medical-history/patient-medical-history.component';
import { PatientProfileComponent } from './patient/patient-profile/patient-profile.component';
import { SettingSidenavComponent } from './settings/setting-sidenav/setting-sidenav.component';
import { GoogleChartsModule } from "angular-google-charts";
@NgModule({
    imports: [
        DashboardRoutingModule,
        CommonModule,
        CommonMaterialModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatCardModule,
        MatSelectModule,
        MatInputModule,
        GoogleChartsModule,
        FormsModule,
        MatTabsModule,
        TimepickerModule.forRoot(),
    ],
    declarations: [
        DashboardComponent,
        DashboardflexComponent,
        SideNavigationComponent,
        ActionToolbarComponent,
        AppointmentListComponent,
        DeclineDialogComponent,
        RescheduleDialogComponent,
        MedicalHistoryComponent,
        EHRSidebarComponent,
        HealthVitalsComponent,
        PrescriptionComponent,
        AddDrugsComponent,
        DiagnosisComponent,
        AddFieldsComponent,
        AllergiesComponent,
        ConditionsComponent,
        OrderLabTestComponent,
        AppointmentSummaryComponent,
        EhrActionBarComponent,
        NavdrawerComponent,
        SetFollowupAppointmentComponent,
        VideoConsultingComponent,
        CallDialogComponent,
        ClinicDashboardComponent,        
        ProfilePageComponent,
        ConfigureFormComponent,
        AddPatientComponent,
        AddDrugsComponent,
        AddEducationComponent,
        AddFieldsComponent,
        AddLabReportsComponent,
        NotificationComponent,
        PatientAppointmentHistoryComponent,
        PatientHistorySidenavComponent,
        PatientLabReportsComponent,
        PatientListComponent,
        SettingSidenavComponent,
        PatientMedicalHistoryComponent,
        PatientProfileComponent
    ],
    exports: [
        DashboardflexComponent
    ],
    entryComponents: [                
        AddEducationComponent,
        DeclineDialogComponent,
        RescheduleDialogComponent,
        AddDrugsComponent,
        AddLabReportsComponent,
        SetFollowupAppointmentComponent,
        CallDialogComponent,
        RescheduleDialogComponent,
        AddDrugsComponent,
        SetFollowupAppointmentComponent,
        CallDialogComponent
    ],
    providers: []
})
export class DashboardModule { }
