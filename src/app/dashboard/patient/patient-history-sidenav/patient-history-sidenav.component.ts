import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonServiceService } from '../../../services/common/common-service.service';
import { PatientService } from '../../../services/patient/patient.service';
@Component({
	selector: 'app-patient-history-sidenav',
	templateUrl: './patient-history-sidenav.component.html',
	styleUrls: ['./patient-history-sidenav.component.scss']
})
export class PatientHistorySidenavComponent implements OnInit {
	
	is_multitenantActive: boolean =  false;
	userId:string = '';
	patientInfo:any;
	profile_image_url:string = '../../../../assets/images/default-user.jpeg';
	autoFocus = {
		profile:false,
		medical:false,
		appointment:false,
		reports:false
	}
	constructor(
		private _router:Router,
		private commonService:CommonServiceService,
		private route:ActivatedRoute,
		private patientService:PatientService		
	) { }

	ngOnInit() {
		console.log("check...");
		console.log(this._router.url);
		// var item = this.commonService.getCurrentItemList();
		// if(item['pid'] != null)
		// 	this.userId = item['pid'];		
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			let hUnicode = this.route.parent.params['value'].hospital_unicode;
			let dUnicode = this.route.parent.params['value'].doctor_unicode;			
			if( (hUnicode == window.localStorage.getItem('hosp_unicode')) && (dUnicode == window.localStorage.getItem('doc_unicode')) ){
				this.is_multitenantActive = true;
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this._router.navigateByUrl('');
			}
		}
		this.getPatientInformation(this.userId);
	}	

	getPatientInformation(patient_id){
		this.patientService.getPatientInfoByPatientId(patient_id).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success === '1'){
					console.log("getPatientInfoByPatientId()", response.data);
					this.patientInfo = response.data;
					this.profile_image_url = (this.patientInfo.profile_image_url) ? this.patientInfo.profile_image_url : this.profile_image_url;
					// this.commonService.showSuccess(response.user_friendly_message);
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	navigateToPatientProfile(){
		console.log("checl ishf rhia");

		if(this.is_multitenantActive){
			let hCode = window.localStorage.getItem('hosp_unicode');
			let dCode = window.localStorage.getItem('doc_unicode');
			this._router.navigateByUrl('/hospital/'+hCode+'/'+dCode+'/patientprofile');
		}else
			this._router.navigateByUrl('/dashboard/patientprofile')
	}

	navigateToPatientMedicalHistory(){

		if(this.is_multitenantActive){
			let hCode = window.localStorage.getItem('hosp_unicode');
			let dCode = window.localStorage.getItem('doc_unicode');
			this._router.navigateByUrl('/hospital/'+hCode+'/'+dCode+'/patientmedicalhistory');
		}else
			this._router.navigateByUrl('/dashboard/patientmedicalhistory');
	}

	navigateToAppointmenthistory(){
		
		if(this.is_multitenantActive){
			let hCode = window.localStorage.getItem('hosp_unicode');
			let dCode = window.localStorage.getItem('doc_unicode');
			this._router.navigateByUrl('/hospital/'+hCode+'/'+dCode+'/patientappointmenthistory');
		}else
			this._router.navigateByUrl('/dashboard/patientappointmenthistory');
	}

	navigateToLabReports(){
		
		if(this.is_multitenantActive){
			let hCode = window.localStorage.getItem('hosp_unicode');
			let dCode = window.localStorage.getItem('doc_unicode');
			this._router.navigateByUrl('/hospital/'+hCode+'/'+dCode+'/patientlabreports');
		}else
			this._router.navigateByUrl('/dashboard/patientlabreports');
	}

}
