import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientHistorySidenavComponent } from './patient-history-sidenav.component';

describe('PatientHistorySidenavComponent', () => {
  let component: PatientHistorySidenavComponent;
  let fixture: ComponentFixture<PatientHistorySidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientHistorySidenavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientHistorySidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
