import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {PatientService} from '../../../services/patient/patient.service';
import {CommonServiceService} from '../../../services/common/common-service.service';
import { StoreNotificationDataService } from 'src/app/services/store-notification/store-notification-data.service';

@Component({
	selector: 'app-patient-profile',
	templateUrl: './patient-profile.component.html',
	styleUrls: ['./patient-profile.component.scss']
})
export class PatientProfileComponent implements OnInit {
	hUnicode: string = '';
	dUnicode: string = '';
	is_multitenantActive: boolean = false;
	url: string = '';
	patient_id:string = '';
	constructor(
		private commonService:CommonServiceService,
		private patientService:PatientService,
		private route:ActivatedRoute,
		private router:Router,
		private storeNotificationDataService:StoreNotificationDataService
	) { }

	patientInfo:any;

	ngOnInit() {
		// var item = this.commonService.getCurrentItemList();
		// if(item['pid'] != null)
		// 	this.patient_id = item['pid'];	
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			this.hUnicode = this.route.parent.params['value'].hospital_unicode;
			this.dUnicode = this.route.parent.params['value'].doctor_unicode;			
			if( (this.hUnicode == window.localStorage.getItem('hosp_unicode')) && (this.dUnicode == window.localStorage.getItem('doc_unicode')) ){
				this.is_multitenantActive = true;
				this.url += this.hUnicode + "/" + this.dUnicode; 
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this.router.navigateByUrl('');
			}
		}
		this.getPatientInformation(this.patient_id);

	}

	getPatientInformation(patient_id){
		this.patientService.getPatientInfoByPatientId(patient_id).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success === '1'){
					console.log("getPatientInfoByPatientId()" + response.data);
					this.patientInfo = response.data;
					// this.commonService.showSuccess(response.user_friendly_message);
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	getImmunizationSchedule(){
		this.patientService.getImmunizationSchedule().subscribe(
			(response: any) => {
				console.log(response);
				if(response.success === '1'){
					console.log("getImmunizationSchedule()" + response.data);
					// this.commonService.showSuccess(response.user_friendly_message);
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	getPatientSharedHealthLogs(log_id){
		this.patientService.getSharedHealthlogByLogId(log_id).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success === '1'){
					console.log("getSharedHealthlogByLogId()" + response.data);
					// this.commonService.showSuccess(response.user_friendly_message);
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	getPatientAllSharedHealthLogs(patient_id){
		this.patientService.getAllSharedHealthlogByPatientId(patient_id).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success === '1'){
					console.log("getAllSharedHealthlogByPatientId()" + response.data);
					// this.commonService.showSuccess(response.user_friendly_message);
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

}
