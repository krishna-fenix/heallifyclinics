import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonServiceService } from '../../../services/common/common-service.service';
import { PatientService } from '../../../services/patient/patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

export interface AllergiesListing {
	type: string;
	severity: string;
	reactiondescription: string;
}

export interface ConditionListing {
	type: string;
	status: string;
	treatedby: string;
	date: string;
}

export interface MedicationListing {
	drugname: string;
	strength: string;
	frequency: string;
	duration: string;
	totalintake: string;
	status:string;
}

@Component({
	selector: 'app-patient-medical-history',
	templateUrl: './patient-medical-history.component.html',
	styleUrls: ['./patient-medical-history.component.scss']
})
export class PatientMedicalHistoryComponent implements OnInit {

	is_multitenantActive: boolean =  false;
	userId:string = '';
	patientInfo: any;
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	displayedColumns: string[] = ['type', 'severity', 'description'];
	allergiesdataSource = new MatTableDataSource();

	conditionsDisplayedColumns: string[] = ['type', 'status', 'description'];
	conditionsDataSource = new MatTableDataSource();

	medicationDisplayedColumns: string[] = ['drugname', 'strength', 'frequency','duration', 'totalintake', 'status'];
	medicationDataSource = new MatTableDataSource();
	constructor(
		private _router:Router,
		private commonService:CommonServiceService,
		private route:ActivatedRoute,
		private patientService:PatientService
	) { }

	ngOnInit() {
		// var item = this.commonService.getCurrentItemList();
		// if(item['pid'] != null)
		// 	this.userId = item['pid'];
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			let hUnicode = this.route.parent.params['value'].hospital_unicode;
			let dUnicode = this.route.parent.params['value'].doctor_unicode;			
			if( (hUnicode == window.localStorage.getItem('hosp_unicode')) && (dUnicode == window.localStorage.getItem('doc_unicode')) ){
				this.is_multitenantActive = true;
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this._router.navigateByUrl('');
			}
		}
		this.getPatientInformation(this.userId);
	}

	getPatientInformation(patient_id){
		this.patientService.getPatientInfoByPatientId(patient_id).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success === '1'){
					console.log("getPatientInfoByPatientId()",response.data);
					this.patientInfo = response.data;
					this.allergiesdataSource.data = response.data.allergies;									
					this.conditionsDataSource.data = response.data.conditions;									
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

}
