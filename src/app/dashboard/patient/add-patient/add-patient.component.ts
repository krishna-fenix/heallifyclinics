import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { AppointmentService } from '../../../services/appointments/appointment.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { ClinicService } from "../../../services/clinic/clinic.service";
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
	selector: 'app-add-patient',
	templateUrl: './add-patient.component.html',
	styleUrls: ['./add-patient.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class AddPatientComponent implements OnInit {

	is_multitenantActive:boolean = false;
	bookings:any;
	genders = ['Male', 'Female']
	states = ['Mh', 'US', 'UK']
	addPatientForm: any;
	blood_groups:any;
	clinics:any;
	reasons:any;
	submitted:boolean = false;
	today = new Date();
	time = new Date();
	patients:any;
	filteredPatient: any;
	selectedUser:any;
	existingPatient:any;	
	registerNew:boolean = false;
	checkExistence:boolean = false;
	confirmation_user:any;
	country_code = '91';
	hUnicode: any;
	dUnicode: any;
	selectedClinic:any;
	constructor(
		private appointmentService: AppointmentService,
		private commonService:CommonServiceService,
		private clinicService:ClinicService,
		private router:Router,
		private route:ActivatedRoute,
		private localStorage: LocalStorageService
	) { 
		this.today.setDate(this.today.getDate());
	}

	ngOnInit() {		
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			this.hUnicode = this.route.parent.params['value'].hospital_unicode;
			this.dUnicode = this.route.parent.params['value'].doctor_unicode;
			if( (this.hUnicode == window.localStorage.getItem('hosp_unicode')) && (this.dUnicode == window.localStorage.getItem('doc_unicode')) ){
				this.is_multitenantActive = true;
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this.router.navigateByUrl('');
			}
		}
		this.addPatientForm = new FormGroup({
			first_name: new FormControl('', [Validators.required]),
			last_name: new FormControl(''),
			country_code: new FormControl('91',[Validators.required]),
			relation_id:new FormControl(''),
			mobile: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
			dob: new FormControl('',),
			email: new FormControl('',[Validators.email,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
			gender: new FormControl(''),
			address: new FormControl(''),
			city: new FormControl(''),
			state: new FormControl(''),
			zipcode: new FormControl('', [Validators.minLength(6),Validators.maxLength(6)]),
			country: new FormControl(''),
			height: new FormControl(''),
			weight: new FormControl('',[Validators.minLength(3),Validators.maxLength(3),Validators.min(1),Validators.max(500)]),
			bloodgroup: new FormControl(''),
			age: new FormControl('',[Validators.minLength(3),Validators.maxLength(3),Validators.min(1),Validators.max(150)]),			
			clinic_id:new FormControl('', [Validators.required]),
			appointment_reason_id:new FormControl('', [Validators.required]),
			appointment_date:new FormControl(new Date(), [Validators.required]),
			appointment_time:new FormControl('', [Validators.required]),			
			is_family_member: new FormControl(''),
			user_id: new FormControl('')
		})
		this.getRelations();
		this.getBloodGroups();
		this.getCities();
		this.getReasons();
		this.getClinics();
		// var item = this.commonService.getCurrentItemList();
		// if(item['mcid']){
		// 	this.addPatientForm.patchValue({
		// 		clinic_id:item['mcid']
		// 	}) 
		// }
	}

	get patientForm() {
		return this.addPatientForm.controls;
	}

	checkExistingFirst(event){
		console.log(event);
		if(!this.checkExistence)
			this.checkExistence = true;
	}

	displayFn(value): string {		
		return value ? value.patient_name : '';
	}

	searchExistingPatient(value){
		this.appointmentService.findPatients(value).subscribe(
			response => {
				console.log(response);
				if(response.success == '1'){
					this.patients = response.data;
					const filterValue = value.toLowerCase();
					this.filteredPatient = this.patients.filter(option => 
						(option.patient_name != null && option.patient_name.toLowerCase().includes(filterValue)) || (option.mobile != null && option.mobile.toLowerCase().includes(filterValue)) || (option.contact != null && option.contact.toLowerCase().includes(filterValue)));
				}else{
					this.commonService.showError(response.user_friendly_message);;
				}
			},
			error => {
				this.commonService.showError("Something Went Wrong!!!");
			}
		)
	}

	selectedPatient(patient){
		console.log(patient);
		this.selectedUser = patient;
		let patient_number = patient.contact || patient.mobile;
		patient_number = patient.country_code + patient_number
		this.appointmentService.getExistingPatient(patient_number).subscribe(
			response => {
				console.log("response", response);
				if(response.success == '1'){
					this.existingPatient = response.data;
				}else{
					this.commonService.showError(response.user_friendly_message);
				}
			},
			error => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	getConfirmation(user){
		this.confirmation_user = user;
	}

	cancel(){
		this.confirmation_user = false;	
	}

	bookAppointmentForExistingUser(user){
		console.log(user);
		this.submitted = true;
		// if (this.addPatientForm.invalid) {
        //     return;
        // }
		var date = this.addPatientForm.value.appointment_date;		
		var time = this.formatAMPM(this.addPatientForm.value.appointment_time);
		date = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
		this.addPatientForm.patchValue({
			first_name: user.first_name,
			last_name: user.last_name ? user.last_name : '',
			mobile: user.mobile || user.contact,			
			gender: user.gender ? user.gender : '',
			address: user.address ? user.address : '',
			appointment_date: date,
			appointment_time: time
		})
		this.appointmentService.bookAppointment(this.addPatientForm.value).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success == "1"){
					// this.addPatientForm.reset();
					this.checkExistence = false;
					this.submitted = false;
					this.existingPatient = [];
					this.today.setDate(this.today.getDate());
					this.time = new Date();
					this.confirmation_user = false;
					// var currentActive = this.localStorage.get('currentActiveItem');
					// currentActive['aid'] = response.data.appointment_id;
					// this.commonService.setCurrentItemList(currentActive);				
					this.commonService.showSuccess(response.message);
				}else{
					this.commonService.showInfo(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	addFamilyMember(){
		this.registerNew = true;
		this.checkExistence = false;
		this.addPatientForm.patchValue({
			is_family_member: 1,
			user_id: this.selectedUser.user_id
		})
	}

	onDate(event): void {
		console.log(event);
		var data = this._calculateAge(event);
		this.addPatientForm.patchValue({
			age:data
		})
		console.log(data);
	}

	_calculateAge(date) { 
		var ageMS = Date.now() - date.getTime();
		var ageDate = new Date(ageMS); 
		return Math.abs(ageDate.getUTCFullYear() - 1970);
	}

	getClinics() {
		this.clinicService.getClinics().subscribe(
			(data:any) => {
				if(data.success === '1'){
					console.log("getClinics()", data.data);
					this.clinics = data.data;
				} else {
					this.commonService.showError(data.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		);
	}

	getReasons(){
		this.appointmentService.getAppoitmentReasons().subscribe(
			response => {
				console.log(response);
				if(response.success == '1')
					this.reasons = response.data.appointment_reasons;
				else	
					console.log("error");
			},
			error => {
				console.log("Error", error);
			}
		)
	}

	isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}

	getRelations(){
		this.appointmentService.getRelations().subscribe(
			(response:any) => {
				console.log(response.data);
				this.bookings = response.data;
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	getBloodGroups(){
		this.appointmentService.getBloogGroups().subscribe(
			(response:any) => {
				console.log(response.data);
				this.blood_groups = response.data;
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	getCities(){
		this.appointmentService.getCities().subscribe(
			(response:any) => {
				console.log(response.data);
				// this.blood_groups = response.data;
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	cancelApp(){
		// if(this.commonService.isDocMultitenantActive()){
		// 	this.router.navigateByUrl('/hospital/'+this.hUnicode+'/'+this.dUnicode+'/dashboard');
		// }else{
			this.router.navigateByUrl('/dashboard/clinic-dashboard');
		// }
	}

	registerPatient(data) {
		console.log(this.addPatientForm.value);
		this.submitted = true;
		if (this.addPatientForm.invalid) {
            return;
        }
		var date = this.addPatientForm.value.appointment_date;		
		this.addPatientForm.value.appointment_time = this.formatAMPM(this.addPatientForm.value.appointment_time);
		date = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
		if(this.addPatientForm.value.dob){
			var dob = this.addPatientForm.value.dob;
			dob = dob.getDate() + "-" + (dob.getMonth()+1) + "-" + dob.getFullYear();
		}
		this.addPatientForm.value.appointment_date = date;
		this.addPatientForm.value.dob = dob;
		console.log(this.addPatientForm.value);
		// if(this.commonService.isDocMultitenantActive()){
		// 	this.addPatientForm.value['unicode'] = window.localStorage.getItem('hosp_unicode');
		// 	this.addPatientForm.value['doctor_unicode'] = window.localStorage.getItem('doc_unicode');
		// }
		this.appointmentService.bookAppointment(this.addPatientForm.value).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success == "1"){
					this.submitted = false;
					this.addPatientForm.reset();
					if(data){						
						// var currentActive = this.localStorage.get('currentActiveItem');
						// currentActive['aid'] = response.data.appointment_id;
						// this.commonService.setCurrentItemList(currentActive);
						// if(this.commonService.isDocMultitenantActive()){
						// 	this.router.navigateByUrl('/hospital/'+this.hUnicode+'/'+this.dUnicode+'/medicalhistory');
						// }else{
							this.router.navigateByUrl('/dashboard/medicalhistory');
						// }
					}
					this.commonService.showSuccess(response.message);
				}else{
					this.commonService.showInfo(response.message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	formatAMPM(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'pm' : 'am';
		hours = hours % 12;
		hours = hours ? hours : 12;
		minutes = minutes < 10 ? '0'+minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	}

}
