import { Component, OnInit, ViewChild } from '@angular/core';
import { PatientService } from 'src/app/services/patient/patient.service';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
export interface ClinicListing {
	srno: number;
	patientname: string;
	contact: number;
	agegender: string;
	lastvisit: string;
	type: string;
}

@Component({
	selector: 'app-patient-list',
	templateUrl: './patient-list.component.html',
	styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {
	displayedColumns: string[] = ['srno', 'patient_name', 'mobile', 'age', 'last_visit', 'type', 'delete'];
	dataSource = new MatTableDataSource();
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;
	is_multitenantActive: boolean = false;

	constructor(
		private patientService:PatientService,
		private commonService:CommonServiceService,
		private router:Router,
		private route:ActivatedRoute,
		private localStorage: LocalStorageService		
	) { }

	ngOnInit() {
		this.loadPatients();
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			let hUnicode = this.route.parent.params['value'].hospital_unicode;
			let dUnicode = this.route.parent.params['value'].doctor_unicode;
			if( (hUnicode == window.localStorage.getItem('hosp_unicode')) && (dUnicode == window.localStorage.getItem('doc_unicode')) ){
				this.is_multitenantActive = true;
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this.router.navigateByUrl('');
			}
		}
	}


	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
    }

	navigateToPatientHistory(user_id) {
		// var currentActive = this.localStorage.get('currentActiveItem');
		// currentActive['pid'] = user_id;
		// this.commonService.setCurrentItemList(currentActive);		
		if(this.is_multitenantActive){
			let hCode = window.localStorage.getItem('hosp_unicode');
			let dCode = window.localStorage.getItem('doc_unicode');
			this.router.navigateByUrl('/hospital/'+hCode+'/'+dCode+'/patientprofile');
		}else{
			this.commonService.navigateByUrl('/dashboard/patientprofile')
		}
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	loadPatients() {
		this.patientService.getPatients().subscribe(
			(data:any) => {
				if(data.success === '1'){
					console.log("getPatients()"+data.data);
					this.dataSource.data = data.data;
					// this.commonService.showSuccess(data.user_friendly_message);

				} else {
					this.commonService.showError(data.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		);
	}


}
