import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientLabReportsComponent } from './patient-lab-reports.component';

describe('PatientLabReportsComponent', () => {
  let component: PatientLabReportsComponent;
  let fixture: ComponentFixture<PatientLabReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientLabReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientLabReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
