import { Component, OnInit } from '@angular/core';
import { AddLabReportsComponent } from '../add-lab-reports/add-lab-reports.component';
import { MatDialog } from '@angular/material';
import { PatientService } from 'src/app/services/patient/patient.service';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { ActivatedRoute, Router } from '@angular/router';

export interface AppointmentListing {
	health: string;
	readings: string;
	treatedby: string;
	date: string;
}

const appointment_data: AppointmentListing[] = [
	{ health: 'Heallify', readings: '1234567890', treatedby: 'asdasd', date: 'Paid' },
	{ health: 'Heallify', readings: '1234567890', treatedby: 'asdasd', date: 'Paid' },
	{ health: 'Heallify', readings: '1234567890', treatedby: 'asdasd', date: 'Paid' },
	{ health: 'Heallify', readings: '1234567890', treatedby: 'asdasd', date: 'Paid' }
];

@Component({
	selector: 'app-patient-lab-reports',
	templateUrl: './patient-lab-reports.component.html',
	styleUrls: ['./patient-lab-reports.component.scss']
})
export class PatientLabReportsComponent implements OnInit {
	userId: string;
	is_multitenantActive: boolean;
	patientInfo: any;

	constructor(
		public dialog: MatDialog,
		private patientService:PatientService,
		private commonService:CommonServiceService,
		private route:ActivatedRoute,
		private _router:Router
	) { }

	ngOnInit() {
		// var item = this.commonService.getCurrentItemList();
		// if(item['pid'] != null)
		// 	this.userId = item['pid'];
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			let hUnicode = this.route.parent.params['value'].hospital_unicode;
			let dUnicode = this.route.parent.params['value'].doctor_unicode;			
			if( (hUnicode == window.localStorage.getItem('hosp_unicode')) && (dUnicode == window.localStorage.getItem('doc_unicode')) ){
				this.is_multitenantActive = true;
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this._router.navigateByUrl('');
			}
		}
		this.getPatientInformation(this.userId);
	}

	displayedColumns: string[] = ['health', 'readings', 'treatedby', 'date'];
	dataSource = appointment_data;

	openAddLabReports(): void {
		const dialogRef = this.dialog.open(AddLabReportsComponent, {
			width: '587px',
			height: 'fit-content',
		});
	}

	getPatientInformation(patient_id){
		this.patientService.getPatientInfoByPatientId(patient_id).subscribe(
			(response: any) => {
				console.log(response);
				if(response.success === '1'){
					console.log("getPatientInfoByPatientId()", response.data);
					this.patientInfo = response.data;										
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError('Something went wrong!!!');
			}
		)
	}

	getHealthRecords(user_id){
		this.patientService.getHealthRecordsByUserId(user_id).subscribe(
			(response:any) => {
				if(response.success === "1"){
					console.log("getHealthRecordsByUserId()"+ response.data);
				}else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	removeHealthRecords(gallery_item_id){
		this.patientService.removeHealthRecordsByGalleryItemId(gallery_item_id).subscribe(
			(response:any) => {
				if(response.success === "1"){
					console.log("removeHealthRecordsByGalleryItemId()"+ response.data);
					// this.commonService.showSuccess(response.user_friendly_message)
				}else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}	


}
