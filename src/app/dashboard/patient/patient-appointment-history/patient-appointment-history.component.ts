import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonServiceService } from '../../../services/common/common-service.service';
import { PatientService } from '../../../services/patient/patient.service';
import { AppointmentService } from 'src/app/services/appointments/appointment.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material';
import { LocalStorageService } from 'angular-2-local-storage';

export interface AppointmentListing {
	srno: number;
	appointmentdate: string;
	time: string;
	type: string;
	payment: string;
	status: string;
}

@Component({
	selector: 'app-patient-appointment-history',
	templateUrl: './patient-appointment-history.component.html',
	styleUrls: ['./patient-appointment-history.component.scss']
})
export class PatientAppointmentHistoryComponent implements OnInit {

	is_multitenantActive: boolean =  false;
	userId:string = '';
	patientApnt: any;
	profile_image_url:string = '../../../../assets/images/default-user.jpeg';
	displayedColumns: string[] = ['srno', 'appointment_date', 'appointment_time', 'type', 'payment', 'actionbutton', 'status'];
	dataSource = new MatTableDataSource;
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;
	hUnicode: any;
	dUnicode: any;
	noData:boolean = false;

	constructor(
		private router:Router,
		private commonService:CommonServiceService,
		private route:ActivatedRoute,
		private patientService:PatientService,
		private appointmentService:AppointmentService,
		private localStorage: LocalStorageService

	) { }

	ngOnInit() {
		// var item = this.commonService.getCurrentItemList();
		// if(item['pid'] != null)
		// 	this.userId = item['pid'];	
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			this.hUnicode = this.route.parent.params['value'].hospital_unicode;
			this.dUnicode = this.route.parent.params['value'].doctor_unicode;			
			if( (this.hUnicode == window.localStorage.getItem('hosp_unicode')) && (this.dUnicode == window.localStorage.getItem('doc_unicode')) ){
				this.is_multitenantActive = true;
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this.router.navigateByUrl('');
			}
		}
		this.getPatientAppoitments(this.userId);
	}	

	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	goToEHR(appointment_id,appointment_status){
		// var currentActive = this.localStorage.get('currentActiveItem');
		// currentActive['aid'] = appointment_id;
		// this.commonService.setCurrentItemList(currentActive);	
		if(appointment_status == '3' || appointment_status != '6'){
			if(this.is_multitenantActive)
				this.router.navigateByUrl('/hospital/' + this.hUnicode + "/" + this.dUnicode + "/medicalhistory");
			else	
				this.router.navigateByUrl('/dashboard/medicalhistory');
		}else{
			if(this.is_multitenantActive)
				this.router.navigateByUrl('/hospital/' + this.hUnicode + "/" + this.dUnicode + "/appointmentsummary");
			else	
				this.router.navigateByUrl('/dashboard/appointmentsummary');
		}		
	}

	getPatientAppoitments(patient_id){
		this.appointmentService.getAllAppointments(patient_id).subscribe(
			response => {
				console.log(response);
				if(response.success == '1'){
					this.dataSource.data = response.data;
					if(response.data.length == 0)
						this.noData = true;					
				}else{
					this.commonService.showError(response.user_friendly_message);
				}
			},
			error => {
				console.log("Error...", error);
				this.commonService.showError("Something Went Wrong!!!");
			}
		)
	}

}
