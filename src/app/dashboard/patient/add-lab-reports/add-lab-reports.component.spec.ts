import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLabReportsComponent } from './add-lab-reports.component';

describe('AddLabReportsComponent', () => {
  let component: AddLabReportsComponent;
  let fixture: ComponentFixture<AddLabReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLabReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLabReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
