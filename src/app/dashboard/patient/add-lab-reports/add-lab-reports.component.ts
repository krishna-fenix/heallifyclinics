import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { PatientService } from '../../../services/patient/patient.service';
import { FormControl, FormGroup, Validators} from '@angular/forms';
@Component({
	selector: 'app-add-lab-reports',
	templateUrl: './add-lab-reports.component.html',
	styleUrls: ['./add-lab-reports.component.scss']
})
export class AddLabReportsComponent implements OnInit {

	tanurl: any;
	report_image: any = this.tanurl;
	filename:any;
	addLabReportForm:FormGroup;
	constructor(
		public dialogRef: MatDialogRef<AddLabReportsComponent>,
		private patientService:PatientService
	) { }

	ngOnInit() {
		this.addLabReportForm = new FormGroup({
			type: new FormControl('', [Validators.required]),
			reading: new FormControl('', [Validators.required]),
			treated_by: new FormControl('', [Validators.required]),
			date: new FormControl(''),
			image: new FormControl('')
		})
	}

	addLabReports(value){
		console.log(value);
		console.log(this.tanurl);
		// this.patientService.addPatientLabReports({}).subscribe(
		// 	response => {
		// 		console.log(response);
		// 	},
		// 	error => {
		// 		console.log(error);
		// 	}
		// )
	}

	onClose() {
		this.dialogRef.close();
	}

	OnUploadSelected(x) {
		document.getElementById(x).click();
	}

	onImageUpload(event) { // called each time file input changes
		if (event.target.files && event.target.files[0]) {
			var reader = new FileReader();
			reader.readAsDataURL(event.target.files[0]); // read file as data url (base64 convertion)
			reader.onload = (event) => { // to reload the image called once readAsDataURL is completed
				this.tanurl = reader.result; 
			}
		}
	}

  }
