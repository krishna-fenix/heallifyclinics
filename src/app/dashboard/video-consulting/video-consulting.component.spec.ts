import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoConsultingComponent } from './video-consulting.component';

describe('VideoConsultingComponent', () => {
  let component: VideoConsultingComponent;
  let fixture: ComponentFixture<VideoConsultingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoConsultingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoConsultingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
