import { Component, OnInit } from '@angular/core';
import { ZoomMtg } from '@zoomus/websdk';
import { CONFIG } from '../../config';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonServiceService } from "src/app/services/common/common-service.service";
import { DoctorCommonService } from 'src/app/services/common/doctor-common.service';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FirebaseMessagingService } from '../../services/firebase-messaging/firebase-messaging.service';
import { StoreNotificationDataService } from '../../services/store-notification/store-notification-data.service';
@Component({
	selector: 'app-video-consulting',
	templateUrl: './video-consulting.component.html',
	styleUrls: ['./video-consulting.component.scss']
})
export class VideoConsultingComponent implements OnInit {

	meetConfig = {
		apiKey: CONFIG.ZOOM_API_KEY,
		apiSecret: CONFIG.ZOOM_API_SECRET,
		meetingNumber: '',
		userName: 'User 2',
		passWord: "",
		leaveUrl: "http://localhost:4200/dashboard",
		role: 1
	};
	callieeMeetingID:any;
	callieeMeetingPassword:any;
	callieeAppointmentID = 123;
	callieeMeeetingJoinUrl:any;
	signature:any;
	totalUserInMeeting:any;
	medicines = ['Paracetomol', 'Rantac', 'Zintax','Zintax'];
	sideBarOptions = {
		pd:false,
		hv:false,
		acd:false,
		pre:false,
		lab:false,
		sum:false
	}
	// 
	allergies:any;
	filteredAllergies:any;
	// 
	conditions:any;
	filteredConditions:any;
	patient_details:any;

	constructor(
		public sanitizer: DomSanitizer,
		private doctorCommonService:DoctorCommonService,
		private commonService:CommonServiceService,
		public dialog: MatDialog,
		private route:ActivatedRoute,
		private firebaseMessagingService:FirebaseMessagingService,
		private storeNotificationDataService:StoreNotificationDataService
	) { }

	ngOnInit() {
		// console.log(this.srcUrl);
		if(this.route.params['value'].meeting_id != undefined){
			this.callieeMeetingID = this.route.params['value'].meeting_id;
			if(this.storeNotificationDataService.storeNotificationData){
				var data = this.storeNotificationDataService.storeNotificationData;
				var url = data.join_url;
				let params = (new URL(url)).searchParams;
				this.callieeMeetingPassword = params.get('pwd');
				this.callieeAppointmentID = data.appointment_id;
				this.callieeMeeetingJoinUrl = data.join_url;
			}
			ZoomMtg.setZoomJSLib('https://source.zoom.us/1.7.5/lib', '/av'); 
			ZoomMtg.preLoadWasm();
			ZoomMtg.prepareJssdk();
			this.joinToZoomMeeting(this.callieeMeetingID, this.callieeMeetingPassword);
			this.getAppointmentDetails();
		}else{
			ZoomMtg.setZoomJSLib('https://source.zoom.us/1.7.5/lib', '/av'); 
			ZoomMtg.preLoadWasm();
			ZoomMtg.prepareJssdk();
			this.initializeZoomMeeting();
		}
	
		// this.openAddCallingDialog(859674);
	}

	initializeZoomMeeting(){
		var formData = new FormData;
		formData.append("appointment_id",'123');
		this.doctorCommonService.createZoomMeeting(formData).subscribe(
			response => {
				console.log(response);
				this.meetConfig.meetingNumber = response.data.id;
				this.meetConfig.passWord = response.data.encrypted_password;
				this.callieeMeeetingJoinUrl = response.data.join_url;
				this.createZoomSignature();
			},	
			error =>{
				console.log("Error on initialize/creating meeting...", error);
			}
		)
	}

	joinToZoomMeeting(meetingID, meetingJoinPassword){
		this.meetConfig.meetingNumber = meetingID;
		this.meetConfig.passWord = meetingJoinPassword;
		this.createZoomSignature();
	}

	createZoomSignature(){
		// var that = this;
		this.signature = ZoomMtg.generateSignature({
			meetingNumber: this.meetConfig.meetingNumber,
			apiKey: this.meetConfig.apiKey,
			apiSecret: this.meetConfig.apiSecret,
			role: this.meetConfig.role,
			success: function(res){
				console.log(res.result);
			}
		});
		this.initializeZoom();
		// if(!this.callieeMeetingID){
		// 	this.sendCallingNotification(this.callieeAppointmentID, this.callieeMeeetingJoinUrl);
		// }
	}	

	initializeZoom(){
		var that = this;
		var count = 1;
		ZoomMtg.init({
			leaveUrl: this.meetConfig.leaveUrl,
			isSupportAV: true,
			success: (res) => {
				console.log("ye hai sign ",this.signature);
				console.log("init success", res);
			  	ZoomMtg.join({
					meetingNumber: this.meetConfig.meetingNumber,
					userName: this.meetConfig.userName,
					signature: this.signature,
					apiKey: this.meetConfig.apiKey,
					passWord:  this.meetConfig.passWord,
					success: (res) => {
						console.log('join meeting success', res);
						ZoomMtg.record({
							record: true
						});									
						//   meeting set to show record button
						ZoomMtg.showRecordFunction({
							show: true
						});

						setInterval(function(){
							ZoomMtg.getAttendeeslist({
								success: function (res) {
									console.log(res, "get getAttendeeslist");
									that.totalUserInMeeting = res.result.attendeesList;
									if(res.result.attendeesList.length > 1 && count == 1){
										count++;
										that.commonService.showInfo("New User Joined!!!");
									}
								}
							});	
						},5000);
								
				
						ZoomMtg.inMeetingServiceListener('onUserJoin', function (data) {
							console.log(data);
						});
					
						ZoomMtg.inMeetingServiceListener('onUserLeave', function (data) {
							console.log(data);
						});
					
						ZoomMtg.inMeetingServiceListener('onUserIsInWaitingRoom', function (data) {
							console.log(data);
						});
					
						ZoomMtg.inMeetingServiceListener('onMeetingStatus', function (data) {
							// {status: 1(connecting), 2(connected), 3(disconnected), 4(reconnecting)}
							console.log(data);
						});
					},
					error: (res) => {
						console.log(res);
					}
				});
			},
			error: (res) => {
			  console.log(res);
			}
		});
		console.log("yaha to aa rha hai...");
		

	}

	sendCallingNotification(appointment_id, join_url){
        var formData = new FormData;
        formData.append("appointment_id", appointment_id);
        formData.append("join_url", join_url);
        this.firebaseMessagingService.sendNotification(formData).subscribe(
            (response) => {
                console.log("Send Notification Response", response);
            },
            (error) => {
                console.log("Error from Send Notification", error);
            }
        )
	}
	
	toggleConsultingOptions(option){
		// document.getElementById("consulting-hidden-panel-toggle").style.width = "350px";
		for (const key in this.sideBarOptions) {
			if (key === option) 
				this.sideBarOptions[key] = true;
			else
				this.sideBarOptions[key] = false;
		}

		var margin = document.getElementById("consulting-hidden-panel-toggle").style.marginRight;
		if(margin != "0px")
			document.getElementById("consulting-hidden-panel-toggle").style.marginRight = "0";
		else
			document.getElementById("consulting-hidden-panel-toggle").style.marginRight = "-350px";
	}

	getAppointmentDetails(){
		this.doctorCommonService.getDoctorFaceSheet(this.callieeAppointmentID).subscribe(
			response => {
				console.log(response);
				if(response.success == "1"){
					this.patient_details = response.data;
				}				
			},
			error => {
				console.log(error);
			}
		)
	}

	getAllergiesAutocomplete(allergies_name){
		this.doctorCommonService.getAllergiesAutocomplete(allergies_name).subscribe(
			(response:any) => {
				console.log(response.data);
				this.allergies = response.data;
				const filterValue = allergies_name.toLowerCase();
				this.filteredAllergies = this.allergies.filter(option => option.name != null && option.name.toLowerCase().includes(filterValue));
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	getConditionsAutocomplete(condition_name){
		this.doctorCommonService.getConditionsAutocomplete(condition_name).subscribe(
			(response:any) => {
				console.log(response);
				this.conditions = response.data;
				const filterValue = condition_name.toLowerCase();
				this.filteredConditions = this.conditions.filter(option => option.name != null && option.name.toLowerCase().includes(filterValue));
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

}
