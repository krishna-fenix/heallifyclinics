import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardMaterialFlexComponent } from './dashboard.component';

describe('DashboardMaterialFlexComponent', () => {
  let component: DashboardMaterialFlexComponent;
  let fixture: ComponentFixture<DashboardMaterialFlexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardMaterialFlexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardMaterialFlexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
