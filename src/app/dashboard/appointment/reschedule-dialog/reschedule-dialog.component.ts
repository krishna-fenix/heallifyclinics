import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AppointmentService } from 'src/app/services/appointments/appointment.service';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
	selector: 'app-reschedule-dialog',
	templateUrl: './reschedule-dialog.component.html',
	styleUrls: ['./reschedule-dialog.component.scss']
})
export class RescheduleDialogComponent implements OnInit {

	rescheduleForm:FormGroup;
	userName:string = '';
	constructor(
		public dialogRef: MatDialogRef<RescheduleDialogComponent>,
		private appointmentService:AppointmentService,
		private commonService:CommonServiceService,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit() {
		console.log(this.data);
		this.rescheduleForm = new FormGroup({
			appointment_date: new FormControl('',[Validators.required]),
			appointment_time: new FormControl('',[Validators.required]),
			appointment_reason: new FormControl('',[Validators.required]),
		})
		this.userName = this.data.element.patient_name;
	}

	onNoClick(){
		this.dialogRef.close();
	}

	rescheduleAppointment(value){
		console.log(value);
		var formData = new FormData;
		formData.append("appointment_id",this.data.element.appointment_id);
		formData.append("appointment_date",value.appointment_date.getDate() + "-"+ (value.appointment_date.getMonth()+1) + "-" + value.appointment_date.getFullYear());
		formData.append("appointment_time",this.formatAMPM(value.appointment_time));
		formData.append("reschedule_reason",value.appointment_reason);
		if(this.data.is_multitenantActive){
			formData.append('unicode', window.localStorage.getItem('hosp_unicode'));
			formData.append('doctor_unicode', window.localStorage.getItem('doc_unicode'));
		}
		this.appointmentService.reschuleAppointment(formData).subscribe(
			response => {
				console.log(response);
				if(response.success == "1"){
					this.commonService.showSuccess("Appointment Rescheduled!!!");
					this.onNoClick();
				}else{
					console.log("error");
				}
			},
			error => {
				console.log("error", error);
			}
		)
	}

	formatAMPM(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'pm' : 'am';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0'+minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	}

}
