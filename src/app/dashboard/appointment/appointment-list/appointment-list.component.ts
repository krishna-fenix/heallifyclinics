import { Component, OnInit, ViewChild} from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatDialog, MatSort } from '@angular/material';
import { DeclineDialogComponent } from "../decline-dialog/decline-dialog.component"
import { RescheduleDialogComponent } from "../reschedule-dialog/reschedule-dialog.component"
import { CommonServiceService } from "../../../services/common/common-service.service";
import { AppointmentService } from 'src/app/services/appointments/appointment.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute} from '@angular/router';
import { StoreNotificationDataService } from 'src/app/services/store-notification/store-notification-data.service';
import { element } from 'protractor';
import { LocalStorageService } from 'angular-2-local-storage';

export interface PeriodicElement {
	srno: string;
	patientname: string;
	age: number;
	date: string;
	time: string;
	type:string;
	payment:string;
	status:string;
}

@Component({
	selector: 'app-appointment-list',
	templateUrl: './appointment-list.component.html',
	styleUrls: ['./appointment-list.component.scss'],
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})

export class AppointmentListComponent implements OnInit {

	is_multitenantActive:boolean = false;
	columnsToDisplay = ['srno', 'patient_name', 'age', 'appointment_date', 'appointment_time', 'type', 'payment_status','action','status' ];
	expandedElement: PeriodicElement | null;
	dataSource = new MatTableDataSource();
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;
	nodata:boolean = false;
	noappointments = {
		today:false,
		upcoming:false,
		past:false
	};
	autoFocusStatus = {
		past:false,
		today:true,
		upcoming:false
	}
	hUnicode: any;
	dUnicode: any;

	constructor(
		public dialog: MatDialog,
		private router:Router,
		private route:ActivatedRoute,
		private appointmentService:AppointmentService,
		private commonService:CommonServiceService,
		private localStorage: LocalStorageService,
		private storeNotificationDataService:StoreNotificationDataService
	) { }

	ngOnInit() {
		this.autoFocusStatus.past = true;
		this.getCurrentAppointments('today');
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			this.hUnicode = this.route.parent.params['value'].hospital_unicode;
			this.dUnicode = this.route.parent.params['value'].doctor_unicode;
			if( (this.hUnicode == window.localStorage.getItem('hosp_unicode')) && (this.dUnicode == window.localStorage.getItem('doc_unicode')) ){
				this.is_multitenantActive = true;
			}else{
				this.localStorage.clearAll();
				this.commonService.showError("Access Not Allowed!!!");
				this.router.navigateByUrl('');
			}
		}
	}
		
	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	openDeclineDialog(element): void {
		var data = {
			element,
			is_multitenantActive:this.is_multitenantActive
		}
		const dialogRef = this.dialog.open(DeclineDialogComponent, {
			width: '342px',
			height: '176px',
			data:data
		});
		dialogRef.afterClosed().subscribe(
			result => {
				this.refreshAppointments();
			}
		)
	}

	openRescheduleDialog(element): void {
		var data = {
			element,
			is_multitenantActive:this.is_multitenantActive
		}
		const dialogRef = this.dialog.open(RescheduleDialogComponent, {
			width: '500px',
			height: 'auto',
			data:data
		});
		dialogRef.afterClosed().subscribe(
			result => {
				this.refreshAppointments();
			}
		)
	}

	navigateToMedicalHistory(elem, status_id){
		this.updateAppointmentStatus(elem.appointment_id, status_id);
		let hCode = window.localStorage.getItem('hosp_unicode');
		let dCode = window.localStorage.getItem('doc_unicode');
		// var item = this.commonService.getCurrentItemList();
		// item['aid'] = elem.appointment_id;
		// this.commonService.setCurrentItemList(item);
		if(elem.type == 'Walk-in'){
			if(this.is_multitenantActive){			
				this.router.navigateByUrl('/hospital/'+hCode+'/'+dCode+'/medicalhistory');
			}else{
				this.commonService.navigateByUrl('/dashboard/medicalhistory');
			}
		}else{
			this.storeNotificationDataService.storeNotificationData = {
				appointment_id: elem.appointment_id
			}
			if(this.is_multitenantActive){			
				this.router.navigateByUrl('/hospital/'+hCode+'/'+dCode+'/consultation');
			}else{
				this.commonService.navigateByUrl('/dashboard/consultation');
			}
		}
	}

	goToEHR(appointment_id, appointment_status){
		// var currentActive = this.localStorage.get('currentActiveItem');
		// currentActive['aid'] = appointment_id;
		// this.commonService.setCurrentItemList(currentActive);
		if(appointment_status == '3'){
			if(this.is_multitenantActive)
				this.router.navigateByUrl('/hospital/' + this.hUnicode + "/" + this.dUnicode + "/medicalhistory");
			else	
				this.router.navigateByUrl('/dashboard/medicalhistory');
		}else{
			if(this.is_multitenantActive)
				this.router.navigateByUrl('/hospital/' + this.hUnicode + "/" + this.dUnicode + "/appointmentsummary");
			else	
				this.router.navigateByUrl('/dashboard/appointmentsummary');
		}
	}
	
	setAutoFocus(value){
		for (const key in this.autoFocusStatus) {
			if (key === value ) 
				this.autoFocusStatus[key] = true;
			else	
				this.autoFocusStatus[key] = false;
		}
	}

	setNoAppointments(value){
		for (const key in this.noappointments) {
			if (key === value ) 
				this.noappointments[key] = true;
			else	
				this.noappointments[key] = false;
		}
	}

	getPastAppointments(autoFocusKey){
		this.nodata = false;
		this.setAutoFocus(autoFocusKey);
		this.appointmentService.getPastAppointments().subscribe(
			(response:any) => {
				if(response.success === '1'){
					console.log("getPastAppointments()", response.data);
					this.dataSource.data = response.data;					
				} else {
					if(response.data.length == 0){
						this.setNoAppointments('past')
						this.nodata = true;
						this.dataSource.data = [];
					}
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error: any) => {
				console.log(error);
				this.commonService.showError("Error getting past appointments!!!");
			}
		)
	}

	getCurrentAppointments(autoFocusKey){
		this.nodata = false;
		this.setAutoFocus(autoFocusKey);
		this.appointmentService.getCurrentAppointments().subscribe(
			(response:any) => {
				if(response.success === '1'){
					console.log("getCurrentAppointments()" + response.data);
					this.dataSource.data = response.data;					
				} else {
					if(response.data.length == 0){
						this.setNoAppointments('today')					
						this.nodata = true;
						this.dataSource.data = [];
					}
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Error getting current appointments!!!");
			}
		)
	}
	  
	getAllAppointments(autoFocusKey){
		this.nodata = false;
		this.setAutoFocus(autoFocusKey);
		this.appointmentService.getAllAppointments().subscribe(
			(response:any) => {
				if(response.success === '1'){
					console.log("getAllAppointments()" + response.data);
					this.dataSource.data = response.data;									
				} else {
					if(response.data.length == 0){
						this.setNoAppointments('upcoming')
						this.nodata = true;
						this.dataSource.data = [];
					}
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Error getting all appointments!!!");
			}
		)
		// this.dataSource.data = this.dataSource.data.filter(e => e.appointment_date > '2019-04-21' );
	}

	getAppointmentReasonsByDoctorId(doctor_id){
		this.appointmentService.getAppointmentReasonsByDoctorId(doctor_id).subscribe(
			(response:any) => {
				if(response.success === '1'){
					console.log("getAppointmentReasonsByDoctorId()" + response.data);
					this.dataSource.data = response.data;
					// this.commonService.showSuccess(response.user_friendly_message);
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Error updating status!!!");
			}
		)
	}

	getClinicStatusForVideoAppointments(){
		this.appointmentService.getVideoAppointmentStatus().subscribe(
			(response:any) => {
				if(response.success === '1'){
					console.log("getVideoAppointmentStatus()" + response.data);
					this.dataSource.data = response.data;
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Error updating status!!!");
			}
		)
	}

	updateAppointmentStatus(appointment_id, status_id){
		var formData = new FormData;
		formData.append("appointment_id", appointment_id);
		if(this.is_multitenantActive){
			formData.append('unicode', window.localStorage.getItem('hosp_unicode'));
			formData.append('doctor_unicode', window.localStorage.getItem('doc_unicode'));
		}
		this.appointmentService.updateAppointmentStatus(status_id,formData).subscribe(
			(response) => {
				console.log(response);
				if(response.success === '1'){
					this.commonService.showSuccess('Appointment Status Changed!!!');
					this.refreshAppointments();
				}else{
					this.commonService.showInfo(response.user_friendly_message);
				}
			},
			(error) => {
				console.log(error);
			}
		)
	}

	refreshAppointments(){
		if(this.autoFocusStatus.past)
			this.getPastAppointments('past');
		else if(this.autoFocusStatus.today)
			this.getCurrentAppointments('today');
		else
			this.getAllAppointments('upcoming');
	}

}