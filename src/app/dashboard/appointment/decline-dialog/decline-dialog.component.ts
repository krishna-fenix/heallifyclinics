import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AppointmentService } from 'src/app/services/appointments/appointment.service';
import { CommonServiceService } from "../../../services/common/common-service.service";
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CONFIG } from 'src/app/config';

@Component({
	selector: 'app-decline-dialog',
	templateUrl: './decline-dialog.component.html',
	styleUrls: ['./decline-dialog.component.scss']
})
export class DeclineDialogComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<DeclineDialogComponent>,
		private appointmentService:AppointmentService,
		private commonService:CommonServiceService,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	userName:string = '';

	ngOnInit() {
		console.log(this.data);
		this.userName = this.data.element.patient_name;
	}

	onNoClick(){
		this.dialogRef.close();
	}

	updateAppointmentStatus(){
		var formData = new FormData;
		formData.append("appointment_id", this.data.element.appointment_id);
		if(this.data.is_multitenantActive){
			formData.append('unicode', window.localStorage.getItem('hosp_unicode'));
			formData.append('doctor_unicode', window.localStorage.getItem('doc_unicode'));
		}
		this.appointmentService.updateAppointmentStatus(CONFIG.APPOINTMENT_STATUS.cancelled, formData).subscribe(
			(response:any) => {
				if(response.success === '1'){
					console.log("updateAppointmentStatus()" + response.data);
					this.commonService.showSuccess('Appointment Status Changed!!!');
					this.onNoClick();
				} else {
					this.commonService.showInfo(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				// this.commonService.showError("Error updating status!!!");
			}
		)
	}

}
