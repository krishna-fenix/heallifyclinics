import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { AppointmentService } from '../../services/appointments/appointment.service';
import { PatientService } from '../../services/patient/patient.service';
import { CONFIG } from 'src/app/config';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonServiceService } from 'src/app/services/common/common-service.service';

@Component({
	selector: 'app-clinic-dashboard',
	templateUrl: './clinic-dashboard.component.html',
	styleUrls: ['./clinic-dashboard.component.scss']
})
export class ClinicDashboardComponent implements OnInit {

	hUnicode: any;
	dUnicode: any;
	is_multitenantActive: boolean;

	currentAppointment:any;
	patientCount:number;
	clinicCount:number;
	appointmentCount:number;
	displayedColumns: string[] = [
		"srno",
		"appointment_date",
		"appointment_time",
		"patient_name",
		"type",
		"status",
		"age"
	];
	nodata:boolean = false;
	dataSource = new MatTableDataSource();
	title = "No. of Patients/Months";
	type = "LineChart";
	data = [
		["Jan", 7],
		["Feb", 6],
		["Mar", 9],
		["Apr", 14],
		["May", 18],
		["Jun", 21],
		["Jul", 25],
		["Aug", 26],
		["Sep", 23],
		["Oct", 18],
		["Nov", 13],
		["Dec", 9]
	];
	columnNames = ["Month", "No. Of patients"];
	options = {
		hAxis: {
			title: "Month"
		},
		vAxis: {
			title: "No. of Patients"
		}
	};
	width = 550;
	height = 459;

	@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
 	@ViewChild(MatSort, {static: true}) sort: MatSort;

	constructor(
		private appointmentService:AppointmentService,		
		private patientService:PatientService,
		private route:ActivatedRoute,
		private router:Router,
		private commonService:CommonServiceService
	) { }

	ngOnInit() {
		this.getCurrentAppointments();
		this.getAllAppointments();
		this.loadPatients();
	}

	ngAfterViewInit(){
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}

	getCurrentAppointments(){
		this.appointmentService.getCurrentAppointments().subscribe(
			response => {
				if(response.success == "1"){
					this.nodata = false;
					this.dataSource.data = response.data;
					this.appointmentCount = response.data.length;
				}else{
					this.appointmentCount = 0;
					this.nodata = true;
					console.log("Error response");
				}
			},
			error => {
				console.log(error);
			}
		)
	}

	getAllAppointments(){
		this.appointmentService.getAllAppointments().subscribe(
			response => {
				// console.log('All Appointments...', response.data);
				if(response.success == "1"){
					
				}else{
					console.log("Error response");
				}
			},
			error => {
				console.log(error);
			}
		)
	}

	loadPatients() {
		this.patientService.getPatients().subscribe(
			(data:any) => {
				if(data.success === '1'){
					// console.log("getPatients()",data.data);
					this.patientCount = data.data.length;					
				} else {
					this.patientCount = 0;
					// this.commonService.showError(data.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				// this.commonService.showError("Something went wrong!!!");
			}
		);
	}

}
