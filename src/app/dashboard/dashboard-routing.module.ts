import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentListComponent } from './appointment/appointment-list/appointment-list.component';
import { MedicalHistoryComponent } from './EHR/medical-history/medical-history.component';
import { HealthVitalsComponent } from './EHR/health-vitals/health-vitals.component';
import { PrescriptionComponent } from './EHR/prescription/prescription.component';
import { DiagnosisComponent } from './EHR/diagnosis/diagnosis.component';
import { AllergiesComponent } from './EHR/allergies/allergies.component';
import { ConditionsComponent } from './EHR/conditions/conditions.component';
import { OrderLabTestComponent } from './EHR/order-lab-test/order-lab-test.component';
import { AppointmentSummaryComponent } from './EHR/appointment-summary/appointment-summary.component';
import { DashboardComponent } from './dashboard.component';
import { VideoConsultingComponent } from './video-consulting/video-consulting.component';
import { AuthGuardDashboardService } from '../services/auth/auth-dashboard/auth-guard-dashboard.service';
import { ProfilePageComponent } from './configure-profile/profile-page/profile-page.component';
import { ConfigureFormComponent } from './configure-profile/configure-form/configure-form.component';
import { AddPatientComponent } from './patient/add-patient/add-patient.component';
import { PatientListComponent } from './patient/patient-list/patient-list.component';
import { NotificationComponent } from './settings/notification/notification.component';
import { PatientProfileComponent } from './patient/patient-profile/patient-profile.component';
import { PatientMedicalHistoryComponent } from './patient/patient-medical-history/patient-medical-history.component';
import { PatientAppointmentHistoryComponent } from './patient/patient-appointment-history/patient-appointment-history.component';
import { PatientLabReportsComponent } from './patient/patient-lab-reports/patient-lab-reports.component';
import { ClinicDashboardComponent } from '../dashboard/clinic-dashboard/clinic-dashboard.component';

const routes: Routes = [
    {
        path: 'dashboard', 
        component: DashboardComponent, 
        canActivate: [AuthGuardDashboardService],
        children: [
            { path: 'clinic-dashboard', component: ClinicDashboardComponent},                               
            { path: 'appointment', component: AppointmentListComponent },
            { path: 'profilepage', component: ProfilePageComponent },
            { path: 'configureprofile', component: ConfigureFormComponent },
            { path: 'addpatient', component: AddPatientComponent },            
            { path: 'medicalhistory', component: MedicalHistoryComponent },
            { path: 'healthvital', component: HealthVitalsComponent },
            { path: 'prescription', component: PrescriptionComponent },
            { path: 'diagnosis', component: DiagnosisComponent },
            { path: 'allergies', component: AllergiesComponent },
            { path: 'conditions', component: ConditionsComponent },
            { path: 'labtests', component: OrderLabTestComponent },
            { path: 'appointmentsummary', component: AppointmentSummaryComponent },
            { path: 'notification', component: NotificationComponent },            
            { path: 'patientlist', component: PatientListComponent },               
            { path: 'patientprofile', component: PatientProfileComponent },
            { path: 'patientmedicalhistory', component: PatientMedicalHistoryComponent },
            { path: 'patientappointmenthistory', component: PatientAppointmentHistoryComponent },
            { path: 'patientlabreports', component: PatientLabReportsComponent},                
            { path: 'consultation', component: VideoConsultingComponent},
            { path: '**', pathMatch: 'full', redirectTo: 'appointment' }
        ]
    },
];
export const DashboardRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);