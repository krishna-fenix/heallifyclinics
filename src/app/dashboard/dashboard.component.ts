import { Component, OnInit } from '@angular/core';
import {FirebaseMessagingService} from '../../app/services/firebase-messaging/firebase-messaging.service'

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
	
	message:any;

	constructor(private firebasemessagingService: FirebaseMessagingService) { }

	ngOnInit() {
		const userId = 'user001';
		this.firebasemessagingService.requestPermission(userId)
		this.firebasemessagingService.receiveMessage()
		this.message = this.firebasemessagingService.currentMessage
		console.log(this.message);
	}

}
