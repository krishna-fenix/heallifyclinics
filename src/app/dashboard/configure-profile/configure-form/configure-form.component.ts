import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AddEducationComponent } from '../add-education/add-education.component';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonServiceService } from "../../../services/common/common-service.service";
import { ConfigureProfileService } from 'src/app/services/configure-profile/configure-profile.service';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CONFIG } from 'src/app/config';
@Component({
	selector: 'app-configure-form',
	templateUrl: './configure-form.component.html',
	styleUrls: ['./configure-form.component.scss']
})
export class ConfigureFormComponent implements OnInit {

	step = 0;
	isFirstLogin:number;
	url: any;
	@ViewChild('otpcode', { static: false }) otpcode: ElementRef;
	states = ['Mh', 'US', 'UK'];
	genders = ['Male', 'Female']
	enableGetCode:boolean = false;
	enableMobileGetCode:boolean = false;
	enableEmailVerifyBtn:boolean = false;
	enableMobileVerifyBtn:boolean = false;
	verifiedEmail:boolean = false;
	verifiedMobile:boolean = false;
	memberships:any;
	filteredMemberships: any;

	specializations:any;
	filteredSpecilization:any;

	languages:any;
	filterLanguages:any;

	services:any;
	filterServices:any;

	userDetails:any;
	basicDetailsForm:FormGroup;
	email_code:any;
	mobile_code:any;
	userEmail:any;
	userMobile:any;
	userPrevEmail:string = '';
	userPrevMobile:string = '';
	userDob:string = '';
	submitted:boolean = false;
	constructor(
		public dialog: MatDialog,
		private route:ActivatedRoute,
		private router: Router,
		private formBuilder:FormBuilder,
		private commonService:CommonServiceService,
		private profileService: ProfileService,
		private configureProfileService:ConfigureProfileService
	) { }

	ngOnInit() {
		let user = this.commonService.getCurrentUser();
		console.log(user);
		this.isFirstLogin = user['is_first_login'];
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			let hUnicode = this.route.parent.params['value'].hospital_unicode;
			let dUnicode = this.route.parent.params['value'].doctor_unicode;
			if( (hUnicode == window.localStorage.getItem('hosp_unicode')) && (dUnicode == window.localStorage.getItem('doc_unicode')) ){
				// this.is_multitenantActive = true;
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this.commonService.navigateByUrl('');
			}
		}
		this.basicDetailsForm = new FormGroup({
			first_name: new FormControl('', [Validators.required]),
			last_name: new FormControl('', [Validators.required]),
			date_of_birth: new FormControl('', [Validators.required]),
			// email: new FormControl('', [Validators.required]),
			// mobile: new FormControl('', [Validators.required]),
			gender: new FormControl('', [Validators.required]),
			address: new FormControl('', [Validators.required]),
			city: new FormControl('1', [Validators.required]),
			state: new FormControl('1', [Validators.required]),
			zip: new FormControl('1', [Validators.required]),
			country: new FormControl('1', [Validators.required]),
			experience: new FormControl('', [Validators.required]),
			aadhar_id: new FormControl('', [Validators.required]),
			registration_id: new FormControl('', [Validators.required]),
			statement: new FormControl('Nice Doctor!!!', [Validators.required]),
		},{
			updateOn: "blur"
		  })
		this.getAutocompleteValues();
		this.getProfileDetails();
	}

	public hasError = (controlName: string, errorName: string) =>{
	return this.basicDetailsForm.controls[controlName].hasError(errorName);
	}

	get bdForm() {
		return this.basicDetailsForm.controls;
	}

	enableCodeBtn(event){
		console.log(event);
		if(event.target.value != this.userPrevEmail)
			this.enableGetCode = true;
		else		
			this.enableGetCode = false;			
	}

	enableMobileCodeBtn(event){
		console.log(event);
		if(event.target.value != this.userPrevMobile)
			this.enableMobileGetCode = true;
		else
			this.enableMobileGetCode = false;		
	}


	getEmailChangeCode(){
		var formData = new FormData;
		formData.append("old_email", this.userPrevEmail);
		formData.append("email", this.userEmail);
		formData.append("type","2");
		this.configureProfileService.sendVerificationCodeOnMobileChange(formData).subscribe(
			(response) => {
				console.log(response);
				// if(response.success == "1")
				this.enableEmailVerifyBtn = true;
				this.enableGetCode = false;
			},
			(error) => {
				console.log(error);
			}
		)
	}

	verifyEmail(){
		var formData = new FormData;
		formData.append("email_code", this.email_code);
		formData.append("email", this.userEmail);
		formData.append("type","2");
		this.configureProfileService.verifyEmail(formData).subscribe(
			response => {
				console.log(response);
				// if(response.success == "1")
					this.commonService.showSuccess("Email Verified!!!");
					this.verifiedEmail = true;
			},
			error => {
				console.log(error);
			}
		)
	}

	// mobile verification
	getMobileChangeCode(){
		var formData = new FormData;
		formData.append("old_mobile", this.userPrevMobile);
		formData.append("mobile", this.userMobile);
		formData.append("type","2");
		formData.append("country_code","+91");
		this.configureProfileService.sendVerificationCodeOnMobileChange(formData).subscribe(
			(response) => {
				console.log(response);
				// if(response.success == "1")
				this.enableMobileVerifyBtn = true;
				this.enableMobileGetCode = false;
			},
			(error) => {
				console.log(error);
			}
		)
	}

	verifyMobile(){
		var formData = new FormData;
		formData.append("mobile_code", this.mobile_code);
		formData.append("mobile", this.userMobile);
		formData.append("type", CONFIG.USER_TYPES.DOCTOR);
		formData.append("country_code","+91");
		this.configureProfileService.verifyChangedMobileNumber(formData).subscribe(
			response => {
				console.log(response);
				// if(response.success == "1")
				this.verifiedMobile = true;
					this.commonService.showSuccess("Email Verified!!!");
			},
			error => {
				console.log(error);
			}
		)
	}

	displayFn(value): string {
		return value && value.name ? value.name : '';
	}
	
	onSelection(event, value){
		console.log(value);
		switch(event){
			case 'membership':
				this.addMembership(value);
				break
			case 'specialization':
				this.addSpecialization(value);
				break;
			case 'language':
				this.addLanguage(value);
				break;
			default:
				console.log("pass");
				break;
		}
	}

	filterList(listToFilter:string, value:string){
		const filterValue = value.toLowerCase();
		switch(listToFilter){
			case 'membership':
				this.filteredMemberships = this.memberships.filter(option => option.name != null && option.name.toLowerCase().includes(filterValue));
				break;
			case 'specialization':
				this.filteredSpecilization = this.specializations.filter(option => option.name != null && option.name.toLowerCase().includes(filterValue));
				break;
			case 'languages':
				this.filterLanguages = this.languages.filter(option => option.name != null && option.name.toLowerCase().includes(filterValue));
				break;
			case 'services':
				this.filterServices = this.services.filter(option => option.name != null && option.name.toLowerCase().includes(filterValue));
				break;
			default: 
				console.log("pass...");
				break;
		}
	}

	navigate() {
		this.router.navigate(['/profile-page']);
	}

	openAddEduDialog(element): void {
		const dialogRef = this.dialog.open(AddEducationComponent, {
			width: 'auto',
			height: 'fit-content',
			data: element
		});

		dialogRef.afterClosed().subscribe(
			response => {
				if(response)
					this.getProfileDetails();
			}
		)
	}

	onImageUpload(event) { // called each time file input changes
		console.log(event.target.files[0]);
		if (event.target.files && event.target.files[0]) {
			var reader = new FileReader();
			reader.readAsDataURL(event.target.files[0]); // read file as data url (base64 convertion)
			reader.onload = (event) => { // to reload the image called once readAsDataURL is completed
				this.url = reader.result;
				this.uplodImage(this.url);
			}
		}
	}

	uplodImage(file){	
		var data = {
			type: 2,
			image: file
		}
		var formData = new FormData;
		formData.append("type","2");
		formData.append("image",file);
		this.configureProfileService.uploadProfileImage(formData).subscribe(
			response => {
				console.log(response);
				if(response.success == "1"){

				}else{
					console.log("Error on uploading image...",response);
				}
			},
			error => {
				console.log("Error", error);
			}
		)
	}


	keypress(event) {
		let nextField = event.target.nextElementSibling; // get the sibling element

		if (event.target.value.length < 1){ // check the maxLength
			return;
		}else{
			nextField.focus();
		}

	}

	verifyandfocus() {
		this.otpcode.nativeElement.focus();
	}

	setStep(index: number) {
		this.step = index;
	}

	nextStep(value?:string) {
		switch(value){
			case 'basic':
				this.addProfileDetails();
				break;
			default:
				console.log("pass to next..");
				break;
		}
		this.step++;
	}

	prevStep() {
		this.step--;
	}

	getAutocompleteValues(){
		this.configureProfileService.getAutocomplete().subscribe(
			(response: any) => {
				console.log("getAutocomplete()" + response.data);
				this.memberships = response.data.membership;
				this.specializations = response.data.specializations;
				this.languages = response.data.language;
				this.services = response.data.services;
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)

	}

	getProfileDetails() {
		this.profileService.profileDetails().subscribe(
			(data) => {
				console.log(data);
				if (data.success === '1') {
					console.log("in success");
					var userdata = data.data;
					this.userDetails = data.data;
					userdata.date_of_birth = new Date(userdata.date_of_birth);
					console.log(userdata);
					this.setFormData(userdata);
					console.log(this.userDetails);
					// for (const key in this.userDetails) {
					// 	if (this.userDetails.hasOwnProperty(key)) {
					// 		if(key == 'date_of_birth')
					// 			this.basicDetailsForm[key] = new Date(this.userDetails[key]);
					// 		else
					// 			this.basicDetailsForm[key] = this.userDetails[key];
					// 	}
					// }
					this.userPrevEmail = this.userDetails.email;
					this.userPrevMobile = this.userDetails.mobile;
				} else {
					this.commonService.showError(data.user_friendly_message)
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wront!!!");
			}
		)
	}

	setFormData(data){
		this.basicDetailsForm.patchValue({
			first_name:data.first_name,
			last_name:data.last_name,
			date_of_birth: data.date_of_birth,			
			gender: (data.gender == 'm') ? 'Male':'Female',
			address: data.address,
			city: data.city,
			state: data.state,
			zip: data.zip,
			country: data.country,
			experience: data.experience,
			aadhar_id: data.aadhar_id,
			registration_id: data.registration_id

		})
	}


	// addDetails(event:any, value:string){
	// 	if(event.keyCode == 13){
	// 		console.log("on addDetails()")
	// 		switch(value){
	// 			case 'membership':
	// 				this.addMembership();
	// 				break;
	// 			case 'specialization':
	// 				this.addSpecialization();
	// 				break;
	// 			case 'language':
	// 				this.addLanguage();
	// 				break;
	// 			default:
	// 				console.log('pass to next...');
	// 		}
	// 	}
	// }

	isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}

	addProfileDetails(){
		var formData = new FormData;
		this.submitted = true;
		if(this.basicDetailsForm.valid){
			for (const key in this.basicDetailsForm.value) {
				if(key == 'date_of_birth'){
					var dob = this.basicDetailsForm.value[key];
					dob = dob.getFullYear() + "-" + (dob.getMonth()+1) + "-" + dob.getDate();
					formData.append(key, dob);
				}else{
					formData.append(key, this.basicDetailsForm.value[key]);
				}
			}
			this.configureProfileService.addProfileDetails(formData).subscribe(
				(response: any) => {
					console.log("addProfileDetails()", response);
					if(response.success == '1'){
						this.commonService.showSuccess(response.user_friendly_message);
					}else{
						this.commonService.showError(response.user_friendly_message);
					}
				},
				(error:any) => {
					console.log(error);
					this.commonService.showError("Something went wrong!!!");
				}
			)
		}else{			
			this.commonService.showError("Some Fields are missing.");
		}
	}
	
	addSpecialization(specialization_id){
		var formData = new FormData;
		formData.append('specialization_id[]', specialization_id)
		this.configureProfileService.addSpecialization(formData).subscribe(
			(response: any) => {
				console.log("addSpecialization()", response)
				this.getProfileDetails();
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)
	}

	addService(){
		var formData = new FormData;
		this.configureProfileService.addService(formData).subscribe(
			(response: any) => {
				console.log("addService()", response);
				this.getProfileDetails();
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)
	}

	addLanguage(language_id){
		var formData = new FormData;
		formData.append('language_id[]', language_id)
		this.configureProfileService.addLanguage(formData).subscribe(
			(response: any) => {
				console.log("addLanguage()", response);
				this.getProfileDetails();
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)
	}

	addMembership(membership_id){
		var formData = new FormData;
		formData.append('membership_id[]', membership_id)
		this.configureProfileService.addMembership(formData).subscribe(
			(response: any) => {
				console.log("addMembership()", response);
				this.getProfileDetails();
				// this.commonService.showSuccess(response.user_friendly_message);
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)
	}

	removeSpeciliation(id){		
		this.configureProfileService.removeSpecializationById(id).subscribe(
			(response: any) => {
				console.log("removeSpecializationById()" + response)
				if(response.success == '1'){
					this.commonService.showSuccess(response.message);
					this.getProfileDetails();
				}else{
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)
	}

	removeEducationById(id){		
		this.configureProfileService.removeEducationById(id).subscribe(
			response => {
				console.log(response);
				if(response.success == '1'){
					this.commonService.showSuccess("Education Removed!!!");
					this.getProfileDetails();
				}else{
					this.commonService.showError(response.user_friendly_message);
				}
			},
			error => {
				this.commonService.showError("Something Went Wrong!!!");
			}
		)
	}

	removeServiceById(id){		
		this.configureProfileService.removeServiceById(id).subscribe(
			(response: any) => {
				console.log("removeServiceById()" + response)
				if(response.success == '1'){
					this.commonService.showSuccess(response.message);
					this.getProfileDetails();
				}else{
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)
	}

	removeLanguageById(id){		
		this.configureProfileService.removeLanguageById(id).subscribe(
			(response: any) => {
				console.log("removeLanguageById()", response)
				if(response.success == '1'){
					this.commonService.showSuccess(response.message);
					this.getProfileDetails();
				}else{
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)
	}

	removeMembershipById(id){		
		this.configureProfileService.removeMembershipById(id).subscribe(
			(response: any) => {
				console.log("addMembership()",response);
				if(response.success == '1'){
					this.commonService.showSuccess(response.message);
					this.getProfileDetails();
				}else{
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)
	}

	formatAMPM(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'pm' : 'am';
		hours = hours % 12;
		hours = hours ? hours : 12;
		minutes = minutes < 10 ? '0'+minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	}

}
