import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfigureProfileService } from '../../../services/configure-profile/configure-profile.service'
import { CommonServiceService } from "../../../services/common/common-service.service";
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CONFIG } from 'src/app/config';

@Component({
	selector: 'app-add-education',
	templateUrl: './add-education.component.html',
	styleUrls: ['./add-education.component.scss']
})
export class AddEducationComponent implements OnInit {

	addEducationForm: FormGroup;
	isUpdate:boolean = false;
	submitted:boolean = false;
	constructor(
		public dialogRef: MatDialogRef<AddEducationComponent>,
		private configureProfileService:ConfigureProfileService,
		private commonService:CommonServiceService,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit() {
		console.log(this.data);
		this.addEducationForm = new FormGroup({
			institution: new FormControl('',[Validators.required]),
			study: new FormControl('',[Validators.required]),
			year: new FormControl('',[Validators.required, Validators.minLength(4)]),
			type: new FormControl(CONFIG.USER_TYPES.DOCTOR)
		})
		if(this.data){
			this.isUpdate = true;
			this.setEducationFormData(this.data);
		}
	}

	setEducationFormData(data){
		this.addEducationForm.patchValue({
			institution: data.institution,
			study: data.study,
			year: data.year
		})
	}

	onClose() {
		this.dialogRef.close();
	}

	addEducation(){
		this.submitted = true;
		if(this.addEducationForm.invalid){
			return;
		}
		var formValue = this.addEducationForm.value;
		console.log(formValue);
		var formData = new FormData;
		formData.append('institution', formValue.institution);
		formData.append('study', formValue.study);
		formData.append('year', formValue.year);
		formData.append('type', formValue.type);

		this.configureProfileService.addEducation(formData).subscribe(
			(data:any)=>{
				console.log("in education add",data);
				if(data.success === '1'){
					console.log(data.user_friendly_message);
					this.commonService.showSuccess(data.message);
					this.dialogRef.close(true);
				} else {
					this.commonService.showError(data.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}

	updateEducation(){
		var formValue = this.addEducationForm.value;
		console.log(formValue);
		var formData = new FormData;
		formData.append('institution', formValue.institution);
		formData.append('study', formValue.study);
		formData.append('year', formValue.year);
		formData.append('type', formValue.type);		
		this.configureProfileService.updateEducationById(this.data.id, formData).subscribe(
			(response: any) => {
				if(response.success === '1'){
					console.log(response.user_friendly_message);
					this.commonService.showSuccess(response.message);
					this.dialogRef.close(true);
				} else {
					this.commonService.showError(response.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showSuccess("Something went wrong!!!");
			}
		)
	}

}
