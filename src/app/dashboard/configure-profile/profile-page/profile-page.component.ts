import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { CommonServiceService } from "../../../services/common/common-service.service";

@Component({
	selector: 'app-profile-page',
	templateUrl: './profile-page.component.html',
	styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

	data1: any = [];
	hUnicode: any;
	dUnicode: any;
	is_multitenantActive: boolean;
	constructor(
		private profileService: ProfileService,
		private commonService:CommonServiceService,
		private route:ActivatedRoute
	) { }

	ngOnInit() {
		if(this.route.parent.params['value'].hospital_unicode != null && this.route.parent.params['value'].doctor_unicode != null){
			this.hUnicode = this.route.parent.params['value'].hospital_unicode;
			this.dUnicode = this.route.parent.params['value'].doctor_unicode;
			if( (this.hUnicode == window.localStorage.getItem('hosp_unicode')) && (this.dUnicode == window.localStorage.getItem('doc_unicode')) ){
				this.is_multitenantActive = true;
			}else{
				window.localStorage.clear();
				this.commonService.showError("Access Not Allowed!!!");
				this.commonService.navigateByUrl('');
			}
		}
		this.getProfileDetails()
	}

	navigate() {
		if(this.is_multitenantActive){
			this.commonService.navigateByUrl('/hospital/'+this.hUnicode+ '/' + this.dUnicode + '/configureprofile');
		}else{
			this.commonService.navigateByUrl('/dashboard/configureprofile');
		}
	}

	getProfileDetails() {
		this.profileService.profileDetails().subscribe(
			(data) => {
				console.log(data);
				if (data.success === '1') {
					console.log("in success");
					this.data1 = data.data;
				} else {
					this.commonService.showError(data.user_friendly_message)
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wront!!!");
			}
		)
	}

	getDoctorProfileHomePage(){
		this.profileService.getProfileHomePage().subscribe(
			(data) => {
				console.log(data);
				if (data.success === '1') {
					console.log("in success");
					this.data1 = data.data;
				} else {
					this.commonService.showError(data.user_friendly_message)
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wront!!!");
			}
		)
	}

	getDoctorProfileById(doctor_id){
		this.profileService.getProfileDetailsByDoctorId(doctor_id).subscribe(
			(data) => {
				console.log(data);
				if (data.success === '1') {
					console.log("in success");
					this.data1 = data.data;
				} else {
					this.commonService.showError(data.user_friendly_message)
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wront!!!");
			}
		)
	}

}
