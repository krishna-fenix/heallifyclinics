import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DoctorCommonService } from '../../../services/common/doctor-common.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-call-dialog',
	templateUrl: './call-dialog.component.html',
	styleUrls: ['./call-dialog.component.scss']
})
export class CallDialogComponent implements OnInit {

	myAudio:any;
	calling_title:string = '';
	meeting_id:any;
	constructor(
		public dialogRef: MatDialogRef<CallDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private doctorCommonService:DoctorCommonService,
		private _router:Router
	) { 
		this.myAudio = new Audio();
		this.myAudio.src = '../../../assets/audio/Phonering.wav';
		this.myAudio.autoplay = true;
		this.myAudio.loop = true;
		this.myAudio.load();
		console.log(data);
	}

	ngOnInit() {
		var url = this.data.join_url;		
		var last = url.substring(url.lastIndexOf("/") + 1, url.length);
		last = last.split('?');
		this.meeting_id = last[0];
		this.calling_title = this.data.title;
		this.myAudio.play();
	}

	acceptCall(){
		this.myAudio.pause();
		this._router.navigateByUrl('dashboard/consulting/'+this.meeting_id);
		this.dialogRef.close();
	}

	onClose() {
		// this.endMeeting(this.data);
		this.myAudio.pause();
		this.dialogRef.close('declined');
		this._router.navigateByUrl('dashboard/appointment');
	}

	endMeeting(meetingID){
		var formData = new FormData;
		formData.append("meetingId", meetingID);
		// this.doctorCommonService.endZoomMeeting(formData).subscribe(
		// 	(response) => {
		// 		console.log(response);
		// 	},
		// 	(error) => {
		// 		console.log(error);
		// 	}
		// )
	}

}
