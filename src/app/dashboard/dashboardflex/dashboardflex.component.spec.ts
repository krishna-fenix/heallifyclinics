import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardflexComponent } from './dashboardflex.component';

describe('DashboardflexComponent', () => {
  let component: DashboardflexComponent;
  let fixture: ComponentFixture<DashboardflexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardflexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardflexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
