import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { MatDrawer } from '@angular/material';
import { User } from 'src/app/models/user';
import { Subscription } from 'rxjs';
import { LocalStorageService } from 'angular-2-local-storage';
import { LoginService } from '../../services/login.service';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { CommonServiceService } from "../../services/common/common-service.service";

@Component({
	selector: 'app-dashboardflex',
	templateUrl: './dashboardflex.component.html',
	styleUrls: ['./dashboardflex.component.scss']
})
export class DashboardflexComponent implements OnInit {

	@ViewChild('drawer',{static: true}) mDrawer: MatDrawer;

	formButtonXs = true;
	mDoctorName = '';
	mSpecialization = '';
	mUser: User;
	mProfileImageUrl: '';
	mBreadCrumbTitle: '23139';

	private subscription: Subscription;

	@Output()
	sideNav: EventEmitter<any> = new EventEmitter<any>();

	mMedia = 'notxs';

	constructor(
		private mLocalStorage: LocalStorageService,
		private loginService: LoginService,
		private mDrawerService: DrawerService,
		private commonService:CommonServiceService
	) { }

  

	ngOnInit() {
		this.mDrawerService.setDrawer(this.mDrawer);
		this.mDrawerService.toggle(540);
		if (this.commonService.getCurrentUser() == null && this.mLocalStorage.get('user') === null) {
		} else {
			this.commonService.setCurrentUser(this.mLocalStorage.get('user') as User)
			this.mUser = this.commonService.getCurrentUser();
			this.mProfileImageUrl = this.mUser.profile_gallery;
			this.mDoctorName = 'Dr. ' + this.mUser.first_name + ' ' + this.mUser.last_name;
		}
	}

	public OnLogOut() {
		this.loginService.logOut().subscribe(
			() => {
				this.mLocalStorage.clearAll();
				this.commonService.mUser = undefined;
				// this.showInfo('Logged Out', '');
				this.commonService.navigateByUrl('/login');
			}, error => {
				this.mLocalStorage.clearAll();
				this.commonService.mUser = undefined;
				this.commonService.showError(error, '');
				this.commonService.navigateByUrl('/login');
				this.commonService.showError(error, '');
			}
		);
	}
}
