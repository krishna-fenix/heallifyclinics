export interface Autocomplete {
    getKind(): string;
    getTitle(): string;
    getId(): string;
  }
  