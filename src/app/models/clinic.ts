export class Clinic {

    doctor_id: string; //  string; //1, string; //
    clinic_id: string; //  string; //5, string; //
    contact: string; //  string; //875424546, string; //
    alt_contact: string; //  string; //2457654, string; //
    fees: string; //  string; //254, string; //
    accept_app: string; //  string; //0, string; //
    is_active: string; //  string; //1, string; //
    accept_video: string; //  string; //0, string; //
    flat_price: string; //  string; //0, string; //
    video_appointment_note: string; //  string; //,
    is_live: string; //  string; //0, string; //
    name: string; //  string; //demo clinic, string; //
    landmark: string; //  string; //,
    address: string; //  string; //Haut-Uele, Democratic Republic of the Congo, string; //
    latitude: string; //  string; //3.5845154, string; //
    longitude: string; //  string; //28.299435, string; //
    zip: string; //  string; //,
    is_timeslot: string; // 0,
    is_admin: string; //  string; //0
}