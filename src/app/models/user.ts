import { Listing } from './listing';

export class User {
  public aadhar_id;
  public aadhar_verified;
  public country_code;
  public created_at;
  public date_of_birth;
  public doctor_id;
  public email = '';
  public email_code;
  public email_verified;
  public family_members;
  public first_name;
  public forget_code;
  public gcm_bit_registered;
  public gender;
  public last_name;
  public mobile;
  public mobile_code;
  public mobile_verified;
  public profile_gallery;
  public profile_image_url;
  public token = '';
  public type;
  public updated_at;
  public is_verified;//": "1",
  public is_active;//": "1",
  public experience;//": "33",
  public statement;//": "Poool",
  public registration_id;//": "dggdgsysys",
  public address;//": "77",
  public zip;//": null,
  public area_id;//": null,
  public city_id;//": null,
  public state_id;//": null,
  public country_id;//": null,
  public latitude;
  public longitude;
  public specializations: Listing[];
  public languages: Listing[];
  public memberships: Listing[];
  public services: Listing[];
  public education: Education[];
  constructor() {
  }

  public getEmail(): string {
     return this.email;
  }
}

export class Education {
  public id;
  public study;
  public institution;
  public year;
}

