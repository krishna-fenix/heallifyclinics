export class Appointment {

    constructor(
      public patient_name: string,
      public appointment_reason: string,
      public appointment_date: string,
      public appointment_time: string) {
    }
  }
  