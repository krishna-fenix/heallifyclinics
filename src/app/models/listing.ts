import { Autocomplete } from './autocomplete';

export class Listing implements Autocomplete {
    kind = 'listing';
    constructor(public id: string, public name: string, type: string) {
        this.kind = type;
    }

    getTitle(): string {
        return this.name;
    }

    getId(): string {
        return this.id;
    }

     getKind(): string {
        return this.kind;
 }
}
