export class DynamicGrid{   
    type:string;
    description:string;
    severity:string;
    condition:string;
    notes:string;
    labtests:string;
    diagnosis:string;
}