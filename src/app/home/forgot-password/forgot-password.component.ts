import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import {CommonServiceService} from "../../services/common/common-service.service";
@Component({
	selector: 'app-forgot-password',
	templateUrl: './forgot-password.component.html',
	styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

	forgotPasswordForm: any;
	resetPasswordForm: any;
	show: string = 'false';

	constructor(
		public loginService: LoginService, 
		public mFormBuilder: FormBuilder,
		private commonService:CommonServiceService
	) { }

	ngOnInit() {
		this.forgotPasswordForm = new FormGroup(
			{
				mobile: new FormControl('', [Validators.required]),
				type: new FormControl('2'),
			}, 
			{
				updateOn: "blur"
			}
		);
	}

	initResetForm() {
		this.resetPasswordForm = this.mFormBuilder.group({
			'mobile': ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
			'password': ['', Validators.required],
			'forget_code': ['', Validators.required]
		})
	}

	onSendCode() {
		console.log(this.forgotPasswordForm.value)
		this.loginService.sendCode(this.forgotPasswordForm.value).subscribe(
			(data: any) => {
				console.log(data);
				if (data.success === '1') {
					console.log(data.user_friendly_message)
					this.commonService.showSuccess(data.user_friendly_message);
					this.show = 'true';
					this.initResetForm();
				} else {
					console.log(data.user_friendly_message)
					this.commonService.showError(data.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}



}
