import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from '../../services/common/common-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';

@Component({
	selector: 'app-verifymobile',
	templateUrl: './verifymobile.component.html',
	styleUrls: ['./verifymobile.component.scss']
})
export class VerifymobileComponent implements OnInit {
	verifyForm: any;

	constructor(public commonService:CommonServiceService,public loginService: LoginService) { }

	ngOnInit() {

		this.verifyForm = new FormGroup({
			mobile: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
			mobile_code: new FormControl('', [Validators.required]),
			type: new FormControl('2'),
		},{ 
			updateOn: "blur" 
		});
	}

	onSignup(){
		console.log(this.verifyForm.value)
		this.loginService.verifyMobile(this.verifyForm.value).subscribe(
			(data:any)=>{
				console.log(data);
				if (data.success === '1') {
					console.log(data.user_friendly_message)
					this.commonService.showSuccess(data.user_friendly_message);
				} else {
					console.log(data.user_friendly_message)
					this.commonService.showError(data.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

}
