import { Component, OnInit, ElementRef } from '@angular/core';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { LocalStorageService } from 'angular-2-local-storage';
import { CommonServiceService } from "../../services/common/common-service.service";
import { CONFIG } from 'src/app/config';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	
	formLogin: any;

	constructor(
		private loginService: LoginService, 
		private commonService:CommonServiceService, 
		private localStorage: LocalStorageService
	) { }

	ngOnInit() {
		this.formLogin = new FormGroup({
			email: new FormControl('', [Validators.required, Validators.email]),
			password: new FormControl('', [Validators.required]),
			type: new FormControl(CONFIG.USER_TYPES.CLINIC_ADMIN)
		});
	}

	OnLogin() {
		console.log("in login", this.formLogin.value)
		this.loginService.login(this.formLogin.value).subscribe(
			(data: any) => {
				if (data.success === '1') {
					this.commonService.navigateByUrl('/dashboard');
					this.commonService.showSuccess(data.user_friendly_message);
					this.localStorage.clearAll();
					this.commonService.setUserInstance(data);
					this.commonService.saveToken(data.data.token);
				} else {
					this.commonService.showError(data.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		);
	}


}
