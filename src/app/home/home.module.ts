import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule, MatCardModule, MatSelectModule, MatInputModule } from '@angular/material';
import { LoginComponent } from '../home/login/login.component';
import { SignupComponent } from "../home/signup/signup.component";
import { VerifymobileComponent } from '../home/verifymobile/verifymobile.component';
import { ForgotPasswordComponent } from '../home/forgot-password/forgot-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationsComponent } from '../common-ui-components/validations/validations.component';
import { DoctorRegisterComponent } from '../home/doctor-register/doctor-register.component';
import { LoginService } from '../services/login.service';
import { RegisterdoctorService } from '../services/register/register-doctor/registerdoctor.service';
import { AuthGuardLoginService } from '../services/auth/auth-login/auth-guard-login.service';

const routes: any = [
	{ path: 'login', component: LoginComponent,canActivate: [AuthGuardLoginService] },
	{ path: 'signup', component: SignupComponent,canActivate: [AuthGuardLoginService] },
	{ path: 'verifymobile', component: VerifymobileComponent,canActivate: [AuthGuardLoginService] },
	{ path: 'forgetpassword', component: ForgotPasswordComponent,canActivate: [AuthGuardLoginService] },
];

@NgModule({
	declarations: [
		LoginComponent,
		ValidationsComponent,
		SignupComponent,
		VerifymobileComponent,
		ForgotPasswordComponent,
		DoctorRegisterComponent
	],
	imports: [
		RouterModule.forChild(routes),
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatCardModule,
		MatSelectModule,
		MatInputModule
	],
	providers: [
		LoginService,
		RegisterdoctorService
	],
	exports: [
		ValidationsComponent
	]
})
export class HomeModule { }
