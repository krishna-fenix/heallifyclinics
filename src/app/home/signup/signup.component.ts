import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RegisterdoctorService } from 'src/app/services/register/register-doctor/registerdoctor.service';
import { CommonServiceService } from "../../services/common/common-service.service";

@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

	doctorRegisterForm:any
	passwordPattern = true;
	fieldtype: string = "password";

	constructor(
		public registerdoctorservice: RegisterdoctorService,
		private commonService:CommonServiceService
	) { }

	ngOnInit() {
		this.doctorRegisterForm = new FormGroup({
			email: new FormControl('', [Validators.required, Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)]),
			country_code:new FormControl('+91'),
			mobile: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
			password: new FormControl('',[Validators.required, Validators.pattern(/^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/)]),
			type: new FormControl('2'),
			first_name: new FormControl('jaydeep'),
			last_name: new FormControl('gokhale'),
			gender: new FormControl('male'),
			registration_id: new FormControl('1'),
			specialization_id: new FormControl('1')
		},{ 
			updateOn: "blur" 
		});
		// this.countries();
	}

	onSignup(){
		console.log(this.doctorRegisterForm.value)
		this.registerdoctorservice.registerDoctor(this.doctorRegisterForm.value).subscribe(
			(data:any)=>{
				console.log(data);
				if (data.success === '1') {
					console.log(data.user_friendly_message)
					this.commonService.showSuccess(data.user_friendly_message);
					this.commonService.navigateByUrl('verifymobile');
				} else {
					console.log(data.user_friendly_message)
					this.commonService.showError(data.user_friendly_message);
				}
			},
			(error:any) => {
				console.log(error);
				this.commonService.showError("Something went wrong!!!");
			}
		)
	}

	showPassword() {
		if (this.fieldtype === "password") {
			this.fieldtype = "text";
		} else {
			this.fieldtype = "password";
		}
	}

	// countries(){
	//   this.countriesService.fetchCountries().subscribe((data:any)=>{
	//     console.log(JSON.stringify(data.name));
	//   })
	// }

}
