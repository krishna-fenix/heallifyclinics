
import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuardLoginService} from "./services/auth/auth-login/auth-guard-login.service"
import {AuthGuardDashboardService} from "./services/auth/auth-dashboard/auth-guard-dashboard.service"

export const appRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardLoginService],
    loadChildren: 'src/app/home/home.module#HomeModule'
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    canActivate: [AuthGuardDashboardService],
    loadChildren: 'src/app/dashboard/dashboard.module#DashboardModule'
  }
];

export const mainAppRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
