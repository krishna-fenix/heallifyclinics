import { BrowserModule } from '@angular/platform-browser';
import { CommonMaterialModule } from './material-common/common-material.module';
import { NgModule, Injector } from '@angular/core';
import { mainAppRouting, appRoutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AuthGuardLoginService} from "./services/auth/auth-login/auth-guard-login.service"
import { AuthGuardDashboardService } from "./services/auth/auth-dashboard/auth-guard-dashboard.service"
import { HomeModule } from "./home/home.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';
import { HttpModule } from '@angular/http';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { ToastrModule } from 'ngx-toastr';
import { MatInputModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { FirebaseMessagingService } from '../app/services/firebase-messaging/firebase-messaging.service';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';

@NgModule({
	imports: [
		BrowserModule,
		LocalStorageModule.forRoot({
			prefix: 'my-app',
			storageType: 'localStorage'
		}),
		RouterModule.forRoot(appRoutes),
		mainAppRouting,
		BrowserAnimationsModule,
		HttpModule,
		HttpClientModule,
		CommonMaterialModule,
		HomeModule,
		DashboardModule,
		ToastrModule.forRoot(), // ToastrModule added
		MatInputModule,
		ReactiveFormsModule,
		AngularFireDatabaseModule,
		AngularFireAuthModule,
		AngularFireMessagingModule,
		AngularFireModule.initializeApp(environment.firebase),
	],
	declarations: [
		AppComponent
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS, 
			useClass: InterceptorService, 
			multi: true
		},
		AuthGuardLoginService,
		AuthGuardDashboardService,
		LocalStorageService,
		FirebaseMessagingService
	],
	exports:[],
	bootstrap: [AppComponent]
})

export class AppModule {
	static injector: Injector;
	constructor(injector: Injector) {
		AppModule.injector = injector;
	}
}
