// import { CONFIG } from './app/config'
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
    apiKey: "AIzaSyB3IM530LT7yjzrhHlvnLKDP-4D1Svszb8",
    authDomain: "heallify-doctors-web.firebaseapp.com",
    databaseURL: "https://heallify-doctors-web.firebaseio.com",
    projectId: "heallify-doctors-web",
    storageBucket: "heallify-doctors-web.appspot.com",
    messagingSenderId: "199865816134",
    appId: "1:199865816134:web:1b7617aded206e9e67e647"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();